module.exports = app => {
    const athletes = require("../controllers/athlete.controller");

    var routerathletes = require("express").Router();

    // Create a new Athlete
    routerathletes.post("/", athletes.create);

    // Retrieve all Athletes
    routerathletes.get("/", athletes.findAll);

    // Retrieve a single Athlete with id
    routerathletes.get("/:id", athletes.findOne);

    // Update a Athlete with id
    routerathletes.put("/:id", athletes.update);

    // Delete a UAthleteser with id
    routerathletes.delete("/:id", athletes.delete);

    // Delete all Athletes
    routerathletes.delete("/", athletes.deleteAll);

    app.use('/api/athletes', routerathletes);
};