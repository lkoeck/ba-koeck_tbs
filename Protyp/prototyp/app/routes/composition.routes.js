module.exports = app => {
    const compositions = require("../controllers/composition.controller.js");

    var routercompositions = require("express").Router();

    // Create a new Composition
    routercompositions.post("/", compositions.create);

    // Retrieve all Compositions
    routercompositions.get("/", compositions.findAll);

    // Retrieve all published Tutorials
    //router.get("/published", roles.findAllPublished);

    // Retrieve a single Composition with id
    routercompositions.get("/:id", compositions.findOne);

    // Update a Composition with id
    routercompositions.put("/:id", compositions.update);

    // Delete a Composition with id
    routercompositions.delete("/:id", compositions.delete);

    // Delete all Compositions
    routercompositions.delete("/", compositions.deleteAll);

    app.use('/api/compositions', routercompositions);
};