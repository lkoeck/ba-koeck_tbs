module.exports = app => {
    const clubs = require("../controllers/club.controller.js");

    var routerclubs = require("express").Router();

    // Create a new Club
    routerclubs.post("/", clubs.create);

    // Retrieve all Clubs
    routerclubs.get("/", clubs.findAll);

    // Retrieve all published Tutorials
    //router.get("/published", roles.findAllPublished);

    // Retrieve a single Club with id
    routerclubs.get("/:id", clubs.findOne);

    // Update a Club with id
    routerclubs.put("/:id", clubs.update);

    // Delete a Club with id
    routerclubs.delete("/:id", clubs.delete);

    // Delete all Clubs
    routerclubs.delete("/", clubs.deleteAll);

    app.use('/api/clubs', routerclubs);
};