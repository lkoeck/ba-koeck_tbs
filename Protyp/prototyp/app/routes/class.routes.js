module.exports = app => {
    const classes = require("../controllers/class.controller.js");

    var routerclass = require("express").Router();

    // Create a new Class
    routerclass.post("/", classes.create);

    // Retrieve all Classes
    routerclass.get("/", classes.findAll);

    // Retrieve all published Tutorials
    //router.get("/published", roles.findAllPublished);

    // Retrieve a single Class with id
    routerclass.get("/:id", classes.findOne);

    // Update a Class with id
    routerclass.put("/:id", classes.update);

    // Delete a Class with id
    routerclass.delete("/:id", classes.delete);

    // Delete all Classes
    routerclass.delete("/", classes.deleteAll);

    app.use('/api/classes', routerclass);
};