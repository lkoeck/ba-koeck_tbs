module.exports = (app) => {
    const scores = require("../controllers/score.controller");

    var routerscores = require("express").Router();

    // Create a new Score
    routerscores.post("/", scores.create);

    // Retrieve all Scores
    routerscores.get("/", scores.findAll);

    // Retrieve all Scores with given registrationId + roundId 
    routerscores.get("/getAll", scores.getAll);

    routerscores.get("/finalScores", scores.finalScores)

     // Retrieve a single Score with id
     routerscores.get("/:id", scores.findOne);

    // Update a Score with id
    routerscores.put("/:id", scores.update);

    // Delete a Score with id
    routerscores.delete("/:id", scores.delete);

    // Delete all Scores
    routerscores.delete("/", scores.deleteAll);

    app.use('/api/scores', routerscores);
};