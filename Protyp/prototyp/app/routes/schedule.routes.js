module.exports = app => {
    const schedules = require("../controllers/schedule.controller");

    var routerschedules = require("express").Router();

    // Create a new Schedule
    routerschedules.post("/", schedules.create);

    // Retrieve all Schedules
    routerschedules.get("/", schedules.findAll);

    // Retrieve all published Tutorials
    //router.get("/published", roles.findAllPublished);
    routerschedules.get("/active/:courtId",schedules.findActive);

    routerschedules.get("/court/:courtId",schedules.findbyCourt);    

    // Retrieve a single Schedule with id
    routerschedules.get("/:id", schedules.findOne);

    // Update a Schedule with id
    routerschedules.put("/:id", schedules.update);

    // Delete a Schedule with id
    routerschedules.delete("/:id", schedules.delete);

    // Delete all Schedule
    routerschedules.delete("/", schedules.deleteAll);

    app.use('/api/schedules', routerschedules);
};