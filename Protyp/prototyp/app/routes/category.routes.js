module.exports = app => {
    const categories = require("../controllers/category.controller");

    var routecategories = require("express").Router();

    // Create a new Category
    routecategories.post("/", categories.create);

    // Retrieve all Categories
    routecategories.get("/", categories.findAll);

    // Retrieve a single Category with id
    //routecategories.get("/:id", categories.findOne);

    routecategories.get("/:id", categories.getName);
    // Retrieve all published Tutorials
    //router.get("/published", roles.findAllPublished);

    // Update a Category with id
    routecategories.put("/:id", categories.update);

    // Delete a Category with id
    routecategories.delete("/:id", categories.delete);

    // Delete all Categories
    routecategories.delete("/", categories.deleteAll);

    app.use('/api/categories', routecategories);
};