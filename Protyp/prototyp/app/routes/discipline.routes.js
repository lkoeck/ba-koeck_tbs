module.exports = app => {
    const disciplines = require("../controllers/discipline.controller.js");

    var routerdisciplines = require("express").Router();

    // Create a new discipline
    routerdisciplines.post("/", disciplines.create);

    // Retrieve all disciplines
    routerdisciplines.get("/", disciplines.findAll);

    // Retrieve all published Tutorials
    //router.get("/published", roles.findAllPublished);

    // Retrieve a single discipline with id
    routerdisciplines.get("/:id", disciplines.findOne);

    // Update a discipline with id
    routerdisciplines.put("/:id", disciplines.update);

    // Delete a discipline with id
    routerdisciplines.delete("/:id", disciplines.delete);

    // Delete all disciplines
    routerdisciplines.delete("/", disciplines.deleteAll);

    app.use('/api/disciplines', routerdisciplines);
};