module.exports = app => {
    const athlete_registrations = require("../controllers/athleteregistration.controller");

    var routerathletereg = require("express").Router();


    // Retrieve all Athlete_registrations
    routerathletereg.get("/", athlete_registrations.findAll);

    // Retrieve a single Athlete_registration with id
    routerathletereg.get("/:id", athlete_registrations.findOne);

    // Update a Athlete_registration with id
    routerathletereg.put("/:id", athlete_registrations.update);

    // Delete a Athlete_registration with id
    routerathletereg.delete("/:id", athlete_registrations.delete);

    // Delete all Athlete_registrations
    routerathletereg.delete("/", athlete_registrations.deleteAll);

    app.use('/api/athleteregistrations', routerathletereg);
};