module.exports = app => {
    const courts = require("../controllers/court.controller.js");

    var routercourt = require("express").Router();

    // Create a new Court
    routercourt.post("/", courts.create);

    // Retrieve all Court
    routercourt.get("/", courts.findAll);

    // Retrieve all published Courts
    //router.get("/published", roles.findAllPublished);

    // Retrieve a single Court with id
    routercourt.get("/:id", courts.findOne);

    // Update a Court with id
    routercourt.put("/:id", courts.update);

    // Delete a Court with id
    routercourt.delete("/:id", courts.delete);

    // Delete all Courts
    routercourt.delete("/", courts.deleteAll);

    app.use('/api/courts', routercourt);
};