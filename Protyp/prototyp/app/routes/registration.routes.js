module.exports = app => {
    const registrations = require("../controllers/registration.controller");

    var routeregistrations = require("express").Router();

    // Create a new Registration
    routeregistrations.post("/", registrations.create);

    // Add a User to an existing Registration
    routeregistrations.post("/addathlete/", registrations.addAthlete);

    // Retrieve all Registrations
    routeregistrations.get("/", registrations.findAll);

   // Retrieve active Registrations
   routeregistrations.get("/categories/:category", registrations.findbyCategory);

    // Retrieve active Registrations
    routeregistrations.get("/active/:category", registrations.findActive);

    // Retrieve a single Registration with id
    routeregistrations.get("/:id", registrations.findOne);

    // Update a Registration with id
    routeregistrations.put("/:id", registrations.update);

    // Delete a Registration with id
    routeregistrations.delete("/:id", registrations.delete);

    // Delete all Registrations
    routeregistrations.delete("/", registrations.deleteAll);

    app.use('/api/registrations', routeregistrations);
};