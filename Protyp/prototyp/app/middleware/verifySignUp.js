const db = require("../models");
const ROLE = db.ROLE;
const Referee = db.referees;

checkDuplicateUsername = (req, res, next) => {
  // Username
  Referee.findOne({
    where: {
      username: req.body.username
    }
  }).then(referee => {
    if (referee) {
      res.status(400).send({
        message: "Failed! Referee name is already in use!"
      });
      return;
    }
    next();
  });
};

checkRolesExisted = (req, res, next) => {
  if (req.body.role) {
    if (!ROLE.includes(req.body.role)) {
        res.status(400).send({
          message: "Failed! Role does not exist = " + req.body.role
        });
        return;
      }
  }
  
  next();
};

const verifySignUp = {
  checkDuplicateUsername: checkDuplicateUsername,
  checkRolesExisted: checkRolesExisted
};

module.exports = verifySignUp;