const jwt = require("jsonwebtoken");
const config = require("../config/auth.config.js");
const db = require("../models");
const Referee = db.referees;

verifyToken = (req, res, next) => {
  let token = req.headers["x-access-token"];

  if (!token) {
    return res.status(403).send({
      message: "No token provided!"
    });
  }

  jwt.verify(token, config.secret, (err, decoded) => {
    if (err) {
      return res.status(401).send({
        message: "Unauthorized!"
      });
    }
    req.referee_id = decoded.id;
    next();
  });
};

isAdmin = (req, res, next) => {
  Referee.findByPk(req.referee_id).then(referee => {
    referee.getRole().then(role => {
      if(role.title === "admin"){
        next();
        return;
      }

      res.status(403).send({
        message: "Require Admin Role!"
      });
      return;
    });
  });
};

isReferee = (req, res, next) => {
  Referee.findByPk(req.referee_id).then(referee => {
    referee.getRole().then(role => {
      if(role.title === "referee"){
        next();
        return;
      }

      res.status(403).send({
        message: "Require Referee Role!"
      });
    });
  });
};

const authJwt = {
  verifyToken: verifyToken,
  isAdmin: isAdmin,
  isReferee: isReferee
};
module.exports = authJwt;