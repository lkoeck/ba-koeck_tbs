const db = require("../models");
const Court = db.courts;
const Op = db.Sequelize.Op;

// Create and Save a new Court
exports.create = (req, res) => {
// Validate request
    if (!req.body.courtname) {
        res.status(400).send({
            message: "Courtname can not be empty!"
        });
        return;
    }
    // Create a Court
    const court = {
        courtname: req.body.courtname
    };

    // Save Court in the database
    Court.create(court)
        .then(data => {
            res.send(data);
        })
        .catch(err => {
            res.status(500).send({
                message:
                    err.message || "Some error occurred while creating the Class."
            });
        });
};

// Retrieve all Courts from the database.
exports.findAll = (req, res) => {
    const classname = req.query.classname;
    var condition = classname ? { classname: { [Op.like]: `%${classname}%` } } : null;

    Court.findAll({ where: condition })
        .then(data => {
            res.send(data);
        })
        .catch(err => {
            res.status(500).send({
                message:
                    err.message || "Some error occurred while retrieving Classes."
            });
        });
};

// Find a single Court with an id
exports.findOne = (req, res) => {
    const id = req.params.id;

    Court.findByPk(id)
        .then(data => {
            res.send(data);
        })
        .catch(err => {
            res.status(500).send({
                message: "Error retrieving Court with id=" + id
            });
        });
};

// Update a Court by the id in the request
exports.update = (req, res) => {
    const id = req.params.id;

    Court.update(req.body, {
        where: { id: id }
    })
        .then(num => {
            if (num == 1) {
                res.send({
                    message: "Court was updated successfully."
                });
            } else {
                res.send({
                    message: `Cannot update Court with id=${id}. Maybe Court was not found or req.body is empty!`
                });
            }
        })
        .catch(err => {
            res.status(500).send({
                message: "Error updating Class with id=" + id
            });
        });
};

// Delete a Court with the specified id in the request
exports.delete = (req, res) => {
    const id = req.params.id;

    Court.destroy({
        where: { id: id }
    })
        .then(num => {
            if (num == 1) {
                res.send({
                    message: "Court was deleted successfully!"
                });
            } else {
                res.send({
                    message: `Cannot delete Court with id=${id}. Maybe Court was not found!`
                });
            }
        })
        .catch(err => {
            res.status(500).send({
                message: "Could not delete Court with id=" + id
            });
        });
};

// Delete all Courts from the database.
exports.deleteAll = (req, res) => {
    Court.destroy({
        where: {},
        truncate: false
    })
        .then(nums => {
            res.send({ message: `${nums} Courts were deleted successfully!` });
        })
        .catch(err => {
            res.status(500).send({
                message:
                    err.message || "Some error occurred while removing all Courts."
            });
        });
};

// Find all published Clubs
/*
exports.findAllPublished = (req, res) => {
    Club.findAll({ where: { published: true } })
        .then(data => {
            res.send(data);
        })
        .catch(err => {
            res.status(500).send({
                message:
                    err.message || "Some error occurred while retrieving tutorials."
            });
        });
};


 */