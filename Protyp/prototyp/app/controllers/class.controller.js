const db = require("../models");
const Class = db.classes;
const Op = db.Sequelize.Op;

// Create and Save a new Class
exports.create = (req, res) => {
// Validate request
    if (!req.body.classname) {
        res.status(400).send({
            message: "Classname can not be empty!"
        });
        return;
    }
    // Create a Class
    const classname = {
        classname: req.body.classname
    };

    // Save Class in the database
    Class.create(classname)
        .then(data => {
            res.send(data);
        })
        .catch(err => {
            res.status(500).send({
                message:
                    err.message || "Some error occurred while creating the Class."
            });
        });
};

// Retrieve all Classes from the database.
exports.findAll = (req, res) => {
    const classname = req.query.classname;
    var condition = classname ? { classname: { [Op.like]: `%${classname}%` } } : null;

    Class.findAll({ where: condition })
        .then(data => {
            res.send(data);
        })
        .catch(err => {
            res.status(500).send({
                message:
                    err.message || "Some error occurred while retrieving Classes."
            });
        });
};

// Find a single Club with an id
exports.findOne = (req, res) => {
    const id = req.params.id;

    Class.findByPk(id)
        .then(data => {
            res.send(data);
        })
        .catch(err => {
            res.status(500).send({
                message: "Error retrieving Class with id=" + id
            });
        });
};

// Update a Class by the id in the request
exports.update = (req, res) => {
    const id = req.params.id;

    Class.update(req.body, {
        where: { id: id }
    })
        .then(num => {
            if (num == 1) {
                res.send({
                    message: "Class was updated successfully."
                });
            } else {
                res.send({
                    message: `Cannot update Class with id=${id}. Maybe Class was not found or req.body is empty!`
                });
            }
        })
        .catch(err => {
            res.status(500).send({
                message: "Error updating Class with id=" + id
            });
        });
};

// Delete a Class with the specified id in the request
exports.delete = (req, res) => {
    const id = req.params.id;

    Class.destroy({
        where: { id: id }
    })
        .then(num => {
            if (num == 1) {
                res.send({
                    message: "Class was deleted successfully!"
                });
            } else {
                res.send({
                    message: `Cannot delete Class with id=${id}. Maybe Class was not found!`
                });
            }
        })
        .catch(err => {
            res.status(500).send({
                message: "Could not delete Class with id=" + id
            });
        });
};

// Delete all Classes from the database.
exports.deleteAll = (req, res) => {
    Class.destroy({
        where: {},
        truncate: false
    })
        .then(nums => {
            res.send({ message: `${nums} Classes were deleted successfully!` });
        })
        .catch(err => {
            res.status(500).send({
                message:
                    err.message || "Some error occurred while removing all Classes."
            });
        });
};

// Find all published Clubs
/*
exports.findAllPublished = (req, res) => {
    Club.findAll({ where: { published: true } })
        .then(data => {
            res.send(data);
        })
        .catch(err => {
            res.status(500).send({
                message:
                    err.message || "Some error occurred while retrieving tutorials."
            });
        });
};


 */