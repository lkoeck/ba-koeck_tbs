const db = require("../models");
const Athlete_Registration = db.athlete_registrations;
const Op = db.Sequelize.Op;

// Create and Save a new Athlete_Registration
exports.create = (req, res) => {
// Validate request
    if (!req.body.athlete_id && !req.body.registration_id) {
        res.status(400).send({
            message: "Athlete_if and Registration_id can not be empty!"
        });
        return;
    }
    // Create a Athlete_Registration
    const athleteregistration = {
        user_id: req.body.user_id,
        registration_id: req.body.registration_id
    };

    // Save Athlete_Registration in the database
    Athlete_Registration.create(athleteregistration)
        .then(data => {
            res.send(data);
        })
        .catch(err => {
            res.status(500).send({
                message:
                    err.message || "Some error occurred while creating the User_Registration."
            });
        });
};

// Retrieve all Athlete_Registration from the database.
exports.findAll = (req, res) => {
    Athlete_Registration.findAll()
        .then(data => {
            res.send(data);
        })
        .catch(err => {
            res.status(500).send({
                message:
                    err.message || "Some error occurred while retrieving Athlete_Registration."
            });
        });
};

// Find a single User_Registration with an id
exports.findOne = (req, res) => {
    const id = req.params.id;

    Athlete_Registration.findByPk(id)
        .then(data => {
            res.send(data);
        })
        .catch(err => {
            res.status(500).send({
                message: "Error retrieving Athlete_Registration with id=" + id
            });
        });
};

// Update a Athlete_Registration by the id in the request
exports.update = (req, res) => {
    const id = req.params.id;

    Athlete_Registration.update(req.body, {
        where: { id: id }
    })
        .then(num => {
            if (num == 1) {
                res.send({
                    message: "Athlete_Registration was updated successfully."
                });
            } else {
                res.send({
                    message: `Cannot update Athlete_Registration with id=${id}. Maybe User_Registration was not found or req.body is empty!`
                });
            }
        })
        .catch(err => {
            res.status(500).send({
                message: "Error updating Athlete_Registration with id=" + id
            });
        });
};

// Delete a Athlete_Registration with the specified id in the request
exports.delete = (req, res) => {
    const id = req.params.id;

    Athlete_Registration.destroy({
        where: { id: id }
    })
        .then(num => {
            if (num == 1) {
                res.send({
                    message: "Athlete_Registration was deleted successfully!"
                });
            } else {
                res.send({
                    message: `Cannot delete Athlete_Registration with id=${id}. Maybe User was not found!`
                });
            }
        })
        .catch(err => {
            res.status(500).send({
                message: "Could not delete Athlete_Registration with id=" + id
            });
        });
};

// Delete all Athlete_Registration from the database.
exports.deleteAll = (req, res) => {
    Athlete_Registration.destroy({
        where: {},
        truncate: false
    })
        .then(nums => {
            res.send({ message: `${nums} Athlete_Registrations were deleted successfully!` });
        })
        .catch(err => {
            res.status(500).send({
                message:
                    err.message || "Some error occurred while removing all Athlete_Registration."
            });
        });
};
