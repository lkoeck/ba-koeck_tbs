const db = require("../models");
const Schedule = db.schedules;
const Op = db.Sequelize.Op;

// Create and Save a new Schedule
exports.create = (req, res) => {
// Validate request
    if (!req.body.time || !req.body.categoryId || !req.body.courtId) {
        res.status(400).send({
            message: "A Field in Schedule is empty!"
        });
        return;
    }
    // Create a Schedule
    const schedule = {
        time: req.body.time,
        categoryId: req.body.categoryId,
        courtId: req.body.courtId,
        status: 0 ,
    };

    // Save Schedule in the database
    Schedule.create(schedule)
        .then(data => {
            res.send(data);
        })
        .catch(err => {
            res.status(500).send({
                message:
                    err.message || "Some error occurred while creating the Schedule."
            });
        });
};

// Retrieve all Schedules from the database.
exports.findAll = (req, res) => {

    Schedule.findAll()
        .then(data => {
            res.send(data);
        })
        .catch(err => {
            res.status(500).send({
                message:
                    err.message || "Some error occurred while retrieving Schedule."
            });
        });
};

// Find a single Schedule with an id
exports.findOne = (req, res) => {
    const id = req.params.id;

    Schedule.findByPk(id)
        .then(data => {
            res.send(data);
        })
        .catch(err => {
            res.status(500).send({
                message: "Error retrieving Schedule with id=" + id
            });
        });
};

// Retrieve the Active Schedule from the database.
exports.findActive = (req, res) => {
    //const courtId = req.params.courtId;

    Schedule.findAll({
        where: {
            status: 1,
            courtId: req.params.courtId
        }})
        .then(data => {
            res.send(data);
        })
        .catch(err => {
            res.status(500).send({
                message:
                    err.message || "Some error occurred while retrieving Active Schedule."
            });
        });
};

// Retrieve the Active Schedule from the database.
exports.findbyCourt = (req, res) => {
    //const courtId = req.params.courtId;

    Schedule.findAll({
        where: {
            courtId: req.params.courtId
        },
        order: [
            ['time', 'ASC']
        ],
    })
        .then(data => {
            res.send(data);
        })
        .catch(err => {
            res.status(500).send({
                message:
                    err.message || "Some error occurred while retrieving Active Schedule."
            });
        });
};



// Update a Schedule by the id in the request
exports.update = (req, res) => {
    const id = req.params.id;
    let court = "court" + req.body.courtId;

    Schedule.update(req.body, {
        where: { id: id }
    })
        .then(num => {
            if (num == 1) {
                Schedule.findByPk(id).then(data => {
                    const updatedStatus = data.status;
                    req.app.io.to(court).emit("changedSchedule", data);
                    res.status(200).send({
                        updatedStatus
                    });
                })
            
            } else {
                res.send({
                    message: `Cannot update Schedule with id=${id}. Maybe Schedule was not found or req.body is empty!`
                });
            }
        })
        .catch(err => {
            res.status(500).send({
                message: "Error updating Schedule with id=" + id
            });
        });
};

// Delete a User with the specified id in the request
exports.delete = (req, res) => {
    const id = req.params.id;

    Schedule.destroy({
        where: { id: id }
    })
        .then(num => {
            if (num == 1) {
                res.send({
                    message: "Schedule was deleted successfully!"
                });
            } else {
                res.send({
                    message: `Cannot delete Schedule with id=${id}. Maybe Schedule was not found!`
                });
            }
        })
        .catch(err => {
            res.status(500).send({
                message: "Could not delete Schedule with id=" + id
            });
        });
};

// Delete all Schedules from the database.
exports.deleteAll = (req, res) => {
    Schedule.destroy({
        where: {},
        truncate: false
    })
        .then(nums => {
            res.send({ message: `${nums} Schedules were deleted successfully!` });
        })
        .catch(err => {
            res.status(500).send({
                message:
                    err.message || "Some error occurred while removing all Schedules."
            });
        });
};
