const db = require("../models");
const Club = db.clubs;
const Op = db.Sequelize.Op;

// Create and Save a new Tutorial
exports.create = (req, res) => {
// Validate request
    if (!req.body.clubname) {
        res.status(400).send({
            message: "Clubname can not be empty!"
        });
        return;
    }
    // Create a Club
    const club = {
        clubname: req.body.clubname
    };

    // Save Club in the database
    Club.create(club)
        .then(data => {
            res.send(data);
        })
        .catch(err => {
            res.status(500).send({
                message:
                    err.message || "Some error occurred while creating the Club."
            });
        });
};

// Retrieve all Clubs from the database.
exports.findAll = (req, res) => {
    const clubname = req.query.clubname;
    var condition = clubname ? { clubname: { [Op.like]: `%${clubname}%` } } : null;

    Club.findAll({ where: condition })
        .then(data => {
            res.send(data);
        })
        .catch(err => {
            res.status(500).send({
                message:
                    err.message || "Some error occurred while retrieving Clubs."
            });
        });
};

// Find a single Club with an id
exports.findOne = (req, res) => {
    const id = req.params.id;

    Club.findByPk(id)
        .then(data => {
            res.send(data);
        })
        .catch(err => {
            res.status(500).send({
                message: "Error retrieving Club with id=" + id
            });
        });
};

// Update a Club by the id in the request
exports.update = (req, res) => {
    const id = req.params.id;

    Club.update(req.body, {
        where: { id: id }
    })
        .then(num => {
            if (num == 1) {
                res.send({
                    message: "Club was updated successfully."
                });
            } else {
                res.send({
                    message: `Cannot update Club with id=${id}. Maybe Club was not found or req.body is empty!`
                });
            }
        })
        .catch(err => {
            res.status(500).send({
                message: "Error updating Club with id=" + id
            });
        });
};

// Delete a Club with the specified id in the request
exports.delete = (req, res) => {
    const id = req.params.id;

    Club.destroy({
        where: { id: id }
    })
        .then(num => {
            if (num == 1) {
                res.send({
                    message: "Club was deleted successfully!"
                });
            } else {
                res.send({
                    message: `Cannot delete Club with id=${id}. Maybe Club was not found!`
                });
            }
        })
        .catch(err => {
            res.status(500).send({
                message: "Could not delete Club with id=" + id
            });
        });
};

// Delete all Clubs from the database.
exports.deleteAll = (req, res) => {
    Club.destroy({
        where: {},
        truncate: false
    })
        .then(nums => {
            res.send({ message: `${nums} Clubs were deleted successfully!` });
        })
        .catch(err => {
            res.status(500).send({
                message:
                    err.message || "Some error occurred while removing all clubs."
            });
        });
};

// Find all published Clubs
/*
exports.findAllPublished = (req, res) => {
    Club.findAll({ where: { published: true } })
        .then(data => {
            res.send(data);
        })
        .catch(err => {
            res.status(500).send({
                message:
                    err.message || "Some error occurred while retrieving tutorials."
            });
        });
};


 */