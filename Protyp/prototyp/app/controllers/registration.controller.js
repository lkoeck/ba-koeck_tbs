const db = require("../models");
const Registration = db.registrations;
const Athlete = db.athletes;
const Op = db.Sequelize.Op;

// Create and Save a new Registration
exports.create = (req, res) => {
// Validate request
    if (!req.body.categoryId) {
        console.log(req.body);
        res.status(400).send({
            message: "Category_id cannot be empty"
        });
        return;
    }
    // Create a Registration
    const registration = {
        status: 0, 
        categoryId: req.body.categoryId
    };

    // Save Registration in the database
    Registration.create(registration)
        .then(data => {
            res.send(data);
        })
        .catch(err => {
            res.status(500).send({
                message:
                    err.message || "Some error occurred while creating the Registration."
            });
        });
};

// Add a Athlete_Registration
exports.addAthlete = (req, res) => {
    if (!req.body.athleteId && !req.body.registrationId) {
        res.status(400).send({
            message: "Athleteid and Registrationid can not be empty!"
        });
        return;
    }
    
    return Athlete.findByPk(req.body.athleteId)
    .then((athlete) => {
      if (!athlete) {
        console.log("Athlete not found!");
        return null;
      }
      return Registration.findByPk(req.body.registrationId).then((registration) => {
        if (!registration) {
          console.log("Registration not found!");
          return null;
        }
        //console.log(registration.id);
        //console.log(user.id);
        registration.addAthletes(athlete)
        .then(data => {
            res.send(data);
        })
        .catch(err => {
            res.status(500).send({
                message:
                    err.message || "Some error occurred while adding Athlete to Registration"
            });
        });
      });
    })
    .catch((err) => {
      console.log(">> Error while adding Athlete to Registration: ", err);
    });

}
//

// Retrieve all Registrations from the database.
exports.findAll = (req, res) => {
    //const name = req.query.name;
    //var condition = name ? { name: { [Op.like]: `%${name}%` } } : null;

    Registration.findAll({include: Athlete })
        .then(data => {
            res.send(data);
        })
        .catch(err => {
            res.status(500).send({
                message:
                    err.message || "Some error occurred while retrieving Registrations."
            });
        });
};

// Retrieve all Registrations from the database.
exports.findbyCategory = (req, res) => {
    const cat = req.params.category;
    //var condition = name ? { name: { [Op.like]: `%${name}%` } } : null;

    Registration.findAll({
        where: {
            category_id: cat
        },
        include: [{
            //all: true 
            model: Athlete,
            attributes:['name']
        }] 
    })
        .then(data => {
            res.send(data);
        })
        .catch(err => {
            res.status(500).send({
                message:
                    err.message || "Some error occurred while retrieving active Registrations."
            });
        });
};

// Retrieve all Registrations from the database.
exports.findActive = (req, res) => {
    const cat = req.params.category;
    //console.log(cat);
    //var condition = name ? { name: { [Op.like]: `%${name}%` } } : null;

    Registration.findAll({
        where: {
            categoryId: cat, 
            status: {
                 [Op.or]:[1,2]
            }
        },
        include: {
            all: true 
            //model: Athlete,
            //attributes:['name']
        } 
    })
        .then(data => {
            res.send(data);
        })
        .catch(err => {
            res.status(500).send({
                message:
                    err.message || "Some error occurred while retrieving active Registrations."
            });
        });
};


// Find a single Registration with an id
exports.findOne = (req, res) => {
    const id = req.params.id;

    Registration.findByPk(id)
        .then(data => {
            res.send(data);
        })
        .catch(err => {
            res.status(500).send({
                message: "Error retrieving Registration with id=" + id
            });
        });
};

// Update a Registration by the id in the request
exports.update = (req, res) => {
    const id = req.params.id;
    let court = "court" + req.body.courtId;

    Registration.update(req.body, {
        where: { id: id }
    })
        .then(num => {
            console.log(num);

            if (num == 1) {
                Registration.findByPk(id).then(data => {
                    //console.log(data);
                    const updatedStatus = data.status;
                    req.app.io.to(court).emit("changedRegistration", data);
                    res.send({
                        updatedStatus
                    });
                })
                
            } else {
                res.send({
                    message: `Cannot update Registration with id=${id}. Maybe Registration was not found or req.body is empty!`
                });
            }
        })
        .catch(err => {
            res.status(500).send({
                message: "Error updating Registration with id=" + id
            });
        });
};

// Delete a Registration with the specified id in the request
exports.delete = (req, res) => {
    const id = req.params.id;

    Registration.destroy({
        where: { id: id }
    })
        .then(num => {
            if (num == 1) {
                res.send({
                    message: "Registration was deleted successfully!"
                });
            } else {
                res.send({
                    message: `Cannot delete Registration with id=${id}. Maybe Registration was not found!`
                });
            }
        })
        .catch(err => {
            res.status(500).send({
                message: "Could not delete Registration with id=" + id
            });
        });
};

// Delete all Registrations from the database.
exports.deleteAll = (req, res) => {
    Registration.destroy({
        where: {},
        truncate: false
    })
        .then(nums => {
            res.send({ message: `${nums} Registrations were deleted successfully!` });
        })
        .catch(err => {
            res.status(500).send({
                message:
                    err.message || "Some error occurred while removing all Registrations."
            });
        });
};
