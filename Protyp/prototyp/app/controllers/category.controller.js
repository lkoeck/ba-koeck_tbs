const db = require("../models");
const Category = db.categories;
const Discipline = db.disciplines;
const Class = db.classes;
const Composition = db.compositions;
const Op = db.Sequelize.Op;

// Create and Save a new Category
exports.create = (req, res) => {
// Validate request
    if (!req.body.category || !req.body.disciplineId || !req.body.compositionId || !req.body.classId) {
        res.status(400).send({
            message: "A Field in Category is empty!"
        });
        return;
    }
    // Create a Category
    const category = {
        category: req.body.category,
        disciplineId: req.body.disciplineId,
        classId: req.body.classId,
        compositionId: req.body.compositionId
    };

    // Save Category in the database
    Category.create(category)
        .then(data => {
            res.send(data);
        })
        .catch(err => {
            res.status(500).send({
                message:
                    err.message || "Some error occurred while creating the User."
            });
        });
};

// Retrieve all Categories from the database.
exports.findAll = (req, res) => {
    const category = req.query.category;
    var condition = category ? { category: { [Op.like]: `%${category}%` } } : null;
    //Category.findAll({ where: condition }, {include: Composition})
    Category.findAll( { include: { all: true }})
        .then(data => {
            res.send(data);
        })
        .catch(err => {
            res.status(500).send({
                message:
                    err.message || "Some error occurred while retrieving Categories."
            });
        });
};

// Find a single Category with an id
exports.findOne = (req, res) => {
    const id = req.params.id;

    Category.findByPk(id)
        .then(data => {
            res.send(data);
        })
        .catch(err => {
            res.status(500).send({
                message: "Error retrieving Category with id=" + id
            });
        });
};

// Update a Category by the id in the request
exports.update = (req, res) => {
    const id = req.params.id;

    Category.update(req.body, {
        where: { id: id }
    })
        .then(num => {
            if (num == 1) {
                res.send({
                    message: "Category was updated successfully."
                });
            } else {
                res.send({
                    message: `Cannot update Category with id=${id}. Maybe Category was not found or req.body is empty!`
                });
            }
        })
        .catch(err => {
            res.status(500).send({
                message: "Error updating Category with id=" + id
            });
        });
};

//retrieve the full Category name
exports.getName = (req, res) => {
    const catid = req.params.id;
    //console.log(catid);
    var attributes = ['category','class.classname', 'discipline.discipline', 'composition.composition'];
    Category.findAll({
        attributes: attributes,
        where: {
            id: catid
        },
        include: [
            {
                model: Class,
                //attributes: ['id','classname']
            },
            {
                model: Discipline,
                attributes: ['id','discipline']
            },
       
            {
                model: Composition,
                attributes: ['id','composition']
            }     
        ]
        })
        .then(data => {
            res.send(data);
        })
        .catch(err => {
            res.status(500).send({
                message:
                    err.message || "Some error occurred while retrieving Categories."
            });
        });
};

// Delete a Category with the specified id in the request
exports.delete = (req, res) => {
    const id = req.params.id;

    Category.destroy({
        where: { id: id }
    })
        .then(num => {
            if (num == 1) {
                res.send({
                    message: "Category was deleted successfully!"
                });
            } else {
                res.send({
                    message: `Cannot delete Category with id=${id}. Maybe Category was not found!`
                });
            }
        })
        .catch(err => {
            res.status(500).send({
                message: "Could not delete Club with id=" + id
            });
        });
};

// Delete all Categories from the database.
exports.deleteAll = (req, res) => {
    Category.destroy({
        where: {},
        truncate: false
    })
        .then(nums => {
            res.send({ message: `${nums} Categories were deleted successfully!` });
        })
        .catch(err => {
            res.status(500).send({
                message:
                    err.message || "Some error occurred while removing all Categoroes."
            });
        });
};
