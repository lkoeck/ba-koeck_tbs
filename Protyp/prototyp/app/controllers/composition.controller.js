const db = require("../models");
const Composition = db.compositions;
const Op = db.Sequelize.Op;

// Create and Save a new Composition
exports.create = (req, res) => {
// Validate request
    if (!req.body.composition) {
        res.status(400).send({
            message: "Composition can not be empty!"
        });
        return;
    }
    // Create a Composition
    const composition = {
        composition: req.body.composition
    };

    // Save Class in the database
    Composition.create(composition)
        .then(data => {
            res.send(data);
        })
        .catch(err => {
            res.status(500).send({
                message:
                    err.message || "Some error occurred while creating the Composition."
            });
        });
};

// Retrieve all Compositions from the database.
exports.findAll = (req, res) => {
    const composition = req.query.composition;
    var condition = composition ? { composition: { [Op.like]: `%${composition}%` } } : null;

    Composition.findAll({ where: condition })
        .then(data => {
            res.send(data);
        })
        .catch(err => {
            res.status(500).send({
                message:
                    err.message || "Some error occurred while retrieving Compositions."
            });
        });
};

// Find a single Composition with an id
exports.findOne = (req, res) => {
    const id = req.params.id;

    Composition.findByPk(id)
        .then(data => {
            res.send(data);
        })
        .catch(err => {
            res.status(500).send({
                message: "Error retrieving Composition with id=" + id
            });
        });
};

// Update a Composition by the id in the request
exports.update = (req, res) => {
    const id = req.params.id;

    Composition.update(req.body, {
        where: { id: id }
    })
        .then(num => {
            if (num == 1) {
                res.send({
                    message: "Composition was updated successfully."
                });
            } else {
                res.send({
                    message: `Cannot update Composition with id=${id}. Maybe Composition was not found or req.body is empty!`
                });
            }
        })
        .catch(err => {
            res.status(500).send({
                message: "Error updating Composition with id=" + id
            });
        });
};

// Delete a Composition with the specified id in the request
exports.delete = (req, res) => {
    const id = req.params.id;

    Composition.destroy({
        where: { id: id }
    })
        .then(num => {
            if (num == 1) {
                res.send({
                    message: "Composition was deleted successfully!"
                });
            } else {
                res.send({
                    message: `Cannot delete Composition with id=${id}. Maybe Composition was not found!`
                });
            }
        })
        .catch(err => {
            res.status(500).send({
                message: "Could not delete Composition with id=" + id
            });
        });
};

// Delete all Compositions from the database.
exports.deleteAll = (req, res) => {
    Composition.destroy({
        where: {},
        truncate: false
    })
        .then(nums => {
            res.send({ message: `${nums} Compositions were deleted successfully!` });
        })
        .catch(err => {
            res.status(500).send({
                message:
                    err.message || "Some error occurred while removing all Compositions."
            });
        });
};

// Find all published Clubs
/*
exports.findAllPublished = (req, res) => {
    Club.findAll({ where: { published: true } })
        .then(data => {
            res.send(data);
        })
        .catch(err => {
            res.status(500).send({
                message:
                    err.message || "Some error occurred while retrieving tutorials."
            });
        });
};


 */