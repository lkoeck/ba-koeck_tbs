const db = require("../models");
const Athlete = db.athletes;
const Op = db.Sequelize.Op;

// Create and Save a new Athlete
exports.create = (req, res) => {
// Validate request
    if (!req.body.name || !req.body.dateOfBirth || !req.body.graduation || !req.body.clubId ) {
        res.status(400).send({
            message: "A Field in Athlete is empty!"
        });
        return;
    }
    // Create a Athlete
    const athlete = {
        name: req.body.name,
        dateOfBirth: req.body.dateOfBirth,
        graduation: req.body.graduation,
        clubId: req.body.clubId
    };

    // Save Athlete in the database
    Athlete.create(athlete)
        .then(data => {
            res.send(data);
        })
        .catch(err => {
            res.status(500).send({
                message:
                    err.message || "Some error occurred while creating the Athlete."
            });
        });
};

// Retrieve all Athletes from the database.
exports.findAll = (req, res) => {
    const name = req.query.name;
    var condition = name ? { name: { [Op.like]: `%${name}%` } } : null;

    Athlete.findAll({ where: condition })
        .then(data => {
            res.send(data);
        })
        .catch(err => {
            res.status(500).send({
                message:
                    err.message || "Some error occurred while retrieving Athletes."
            });
        });
};

// Find a single Athlete with an id
exports.findOne = (req, res) => {
    const id = req.params.id;

    Athlete.findByPk(id)
        .then(data => {
            res.send(data);
        })
        .catch(err => {
            res.status(500).send({
                message: "Error retrieving Athlete with id=" + id
            });
        });
};

// Update a Athlete by the id in the request
exports.update = (req, res) => {
    const id = req.params.id;

    Athlete.update(req.body, {
        where: { id: id }
    })
        .then(num => {
            if (num == 1) {
                res.send({
                    message: "Athlete was updated successfully."
                });
            } else {
                res.send({
                    message: `Cannot update Athlete with id=${id}. Maybe Athlete was not found or req.body is empty!`
                });
            }
        })
        .catch(err => {
            res.status(500).send({
                message: "Error updating Athlete with id=" + id
            });
        });
};

// Delete a Athlete with the specified id in the request
exports.delete = (req, res) => {
    const id = req.params.id;

    Athlete.destroy({
        where: { id: id }
    })
        .then(num => {
            if (num == 1) {
                res.send({
                    message: "Athlete was deleted successfully!"
                });
            } else {
                res.send({
                    message: `Cannot delete Athlete with id=${id}. Maybe User was not found!`
                });
            }
        })
        .catch(err => {
            res.status(500).send({
                message: "Could not delete Athlete with id=" + id
            });
        });
};

// Delete all Athletes from the database.
exports.deleteAll = (req, res) => {
    Athlete.destroy({
        where: {},
        truncate: false
    })
        .then(nums => {
            res.send({ message: `${nums} Athletes were deleted successfully!` });
        })
        .catch(err => {
            res.status(500).send({
                message:
                    err.message || "Some error occurred while removing all Athlete."
            });
        });
};
