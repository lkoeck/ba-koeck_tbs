const { sequelize } = require("../models");
const db = require("../models");
const Score = db.scores;
const Op = db.Sequelize.Op;
const Referee = db.referees;
const query = require('../utils/totalScoresQuery.json');

// Create and Save a new Score
exports.create = (req, res) => {
// Validate request
    if (!req.body.registrationId || !req.body.roundId || !req.body.technique || !req.body.presentation || !req.body.refereeId || !req.body.courtId) {
        console.log(req.body);
        res.status(400).send({
            message: "A Field in Score is empty!"
        });
        return;
    }
    // Create a Score
    const score = {
        registrationId: req.body.registrationId ,
        roundId: req.body.roundId,
        technique: req.body.technique,
        presentation: req.body.presentation,
        refereeId: req.body.refereeId,
        refereeName: req.body.refereeName,
        courtId: req.body.courtId
    };

    //todo: how many scores exist, 
    let number = 0;
    Score.findAll({
        where: {
            registration_id: req.body.registrationId ,
            round_id: req.body.roundId
        }
    }).then(data => {
        //console.log("Anzahl Treffer: "+data.length);
        number = data.length;


        if (number < 5) {
            // #scores < 5 -> post scores in DB
            // Save Score in the database
            Score.create(score)
                .then(data => {
                    //Websocket
                    let court = "court" + req.body.courtId;
                    req.app.io.to(court).emit("postedScore",score);
                    if(number == 4){
                         //  scores == 4: increment status of registration
                         req.app.io.to(court).emit("5 Scores detected");
                    }
                    res.send(data);
                })
                .catch(err => {
                    res.status(500).send({
                        message:
                            err.message || "Some error occurred while creating the Score."
                    });
                });
        } else {
            // # scores > 5 dont post scores 
            res.send("There already exist 5 Entries for Registration "+req.body.registrationId+" and Round "+req.body.roundId);
        }
    });
};

// Retrieve all Scores from the database.
exports.findAll = (req, res) => {
    //const name = req.query.name;
    //var condition = name ? { name: { [Op.like]: `%${name}%` } } : null;

    Score.findAll( )
        .then(data => {
            res.send(data);
        })
        .catch(err => {
            res.status(500).send({
                message:
                    err.message || "Some error occurred while retrieving Scores."
            });
        });
};

exports.finalScores = (req, res) => {
    db.sequelize.query(query.query)
    .then(data => {
        res.send(data[0]);
    }).catch(err => {
        res.status(500).send({
            message:
                err.message || "Some error occurred while retrieving Scores."
        });
    })
};

// Find a single Score with an id
exports.findOne = (req, res) => {
    const id = req.params.id;

    Score.findByPk(id)
        .then(data => {
            res.send(data);
        })
        .catch(err => {
            res.status(500).send({
                message: "Error retrieving Score with id=" + id
            });
        });
};

// Retrieve all Scores of a given Id + round*

exports.getAll = (req,res) => {
    if (!req.query.registrationId || !req.query.roundId) {
        console.log(req.body);
        res.status(400).send({
            message: "A Field in Score is empty!"
        });
        return;
    }

    Score.findAll({
        where: {
            registration_id : req.query.registrationId,
            round_id: req.query.roundId
        },
        include: [
           {
                model: Referee,
                //attributes: ['id','classname']
            }]
    }).then(data => {
        res.send(data);
    })
    .catch(err => {
        res.status(500).send({
            message: err.message || "Some error occurred while retrieving Scores."
        });
    });
};

// Update a Score by the id in the request
exports.update = (req, res) => {
    const id = req.params.id;

    Score.update(req.body, {
        where: { id: id }
    })
        .then(num => {
            if (num == 1) {
                res.send({
                    message: "Score was updated successfully."
                });
            } else {
                res.send({
                    message: `Cannot update Score with id=${id}. Maybe Score was not found or req.body is empty!`
                });
            }
        })
        .catch(err => {
            res.status(500).send({
                message: "Error updating Score with id=" + id
            });
        });
};

// Delete a Score with the specified id in the request
exports.delete = (req, res) => {
    const id = req.params.id;

    Score.destroy({
        where: { id: id }
    })
        .then(num => {
            if (num == 1) {
                res.send({
                    message: "Score was deleted successfully!"
                });
            } else {
                res.send({
                    message: `Cannot delete Score with id=${id}. Maybe Score was not found!`
                });
            }
        })
        .catch(err => {
            res.status(500).send({
                message: "Could not delete Score with id=" + id
            });
        });
};

// Delete all Scores from the database.
exports.deleteAll = (req, res) => {
    Score.destroy({
        where: {},
        truncate: false
    })
        .then(nums => {
            res.send({ message: `${nums} Scores were deleted successfully!` });
        })
        .catch(err => {
            res.status(500).send({
                message:
                    err.message || "Some error occurred while removing all Scores."
            });
        });
};


const checkScores = (registration_id, round_id) => {
    Score.findAll({
        where: {
            registration_id: registration_id,
            round_id: round_id
        }
    }).then(data => {
        console.log("Anzahl Treffer: "+data.length);
        return data.length;
    });
}