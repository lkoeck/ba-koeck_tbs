const db = require("../models");
const config = require("../config/auth.config");
const Referee = db.referees;
const Role = db.roles;

const Op = db.Sequelize.Op;

var jwt = require("jsonwebtoken");
var bcrypt = require("bcryptjs");

exports.signup = (req, res) => {
  // Save Referee to Database
    Referee.create({
      username: req.body.username,
      password: bcrypt.hashSync(req.body.password, 8)
    })
    
    .then(referee => {
        const title = req.body.role;
        var condition = title ? {title :{ [Op.like]: `%${title}%` } } : null;

          Role.findOne({where: condition}
          ).then(role => {
            if (!role) {
              console.log("Role not found!");
              return null;
            }
            console.log(role);
            referee.setRole(role)
            .then(() => {
              res.send({ message: "User was registered successfully!" });
            })
            .catch(err =>{
              res.send({ message: "Some error occured while adding Role to referee 1 !" });
            })
          })
          .catch(err =>{
            res.send({ message: "Some error occured while adding Role to referee!" });
          })
      })
      .catch(err => {
        res.status(500).send({ message: err.message });
      });
};

exports.signin = (req, res) => {
  Referee.findOne({
    where: {
      username: req.body.username
    }
  })
    .then(referee => {
      if (!referee) {
        return res.status(404).send({ message: "Referee Not found." });
      }

      var passwordIsValid = bcrypt.compareSync(
        req.body.password,
        referee.password
      );

      if (!passwordIsValid) {
        return res.status(401).send({
          accessToken: null,
          message: "Invalid Password!"
        });
      }

      var token = jwt.sign({ id: referee.id }, config.secret, {
        expiresIn: 86400 // 24 hours
      });

      referee.getRole().then(role => {
        for (let i = 0; i < role.length; i++) {
          authorities.push("ROLE_" + roles[i].title.toUpperCase());
        }
        res.status(200).send({
          id: referee.id,
          username: referee.username,
          role: role.title,
          accessToken: token
        });
      });
    })
    .catch(err => {
      res.status(500).send({ message: err.message });
    });
};