module.exports = (sequelize, Sequelize) => {
    const Referee = sequelize.define("referee", {
      username: {
        type: Sequelize.STRING
      },
      password: {
        type: Sequelize.STRING
      }
    }, {underscored: true});
  
    return Referee;
  };