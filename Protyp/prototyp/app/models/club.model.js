module.exports = (sequelize, Sequelize) => {
    var Club = sequelize.define("club", {
        id: {
            type: Sequelize.INTEGER(11),
            allowNull: false,
            autoIncrement: true,
            primaryKey: true
        },
        clubname: {
            type: Sequelize.STRING
        }
    }, {underscored: true});

    return Club;
};

