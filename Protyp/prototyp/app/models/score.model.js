module.exports = (sequelize, Sequelize) => {
    const Score = sequelize.define("score", {
        id: {
            type: Sequelize.INTEGER(11),
            allowNull: false,
            autoIncrement: true,
            primaryKey: true
        },
        registrationId: {
            //FK in Registration table
            type: Sequelize.INTEGER(11),
        },
        refereeId:{
            //FK in Referee table
            type: Sequelize.INTEGER(11),
        },
        courtId:{
            //FK in Court table
           type: Sequelize.INTEGER(11),
       },
        roundId: {
            type: Sequelize.INTEGER(11),
        },
        technique:{
            type: Sequelize.DOUBLE,
        },
        presentation:{
            type: Sequelize.DOUBLE,
        }
    }, {underscored: true});

    return Score;
}
