module.exports = (sequelize, Sequelize) => {
    const Category = sequelize.define("category", {
        id: {
            type: Sequelize.INTEGER(11),
            allowNull: false,
            autoIncrement: true,
            primaryKey: true
        },
        category: {
            type: Sequelize.STRING
        },
        disciplineId: {
            //FK in Disciplines table
            type: Sequelize.INTEGER(11)
        },
        classId: {
            //FK in Classes table
            type: Sequelize.INTEGER(11)
        },
        compositionId: {
            //FK in Classes table
            type: Sequelize.INTEGER(11)
        }
    }, {underscored: true});

    return Category;
}
