module.exports = (sequelize, Sequelize) => {
    const Athlete = sequelize.define("athlete", {
        id: {
            type: Sequelize.INTEGER(11),
            allowNull: false,
            autoIncrement: true,
            primaryKey: true
        },
        name: {
            type: Sequelize.STRING
        },
        dateOfBirth: {
            type: Sequelize.DATE 
        },
        graduation: {
            type: Sequelize.STRING
        },
        clubId: {
            //FK in Clubs table
            type: Sequelize.INTEGER(11)
        },
    }, {underscored: true});

    return Athlete;
}
