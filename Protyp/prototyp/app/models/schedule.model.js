module.exports = (sequelize, Sequelize) => {
    const Schedule = sequelize.define("schedule", {
        id: {
            type: Sequelize.INTEGER(11),
            allowNull: false,
            autoIncrement: true,
            primaryKey: true
        },
        time: {
            type: Sequelize.DATE 
        },
        categoryId: {
            //FK in category table
            type: Sequelize.INTEGER(11)
        },
        courtId: {
            //FK in court table
            type: Sequelize.INTEGER(11)
        },
        status:{
            type: Sequelize.INTEGER(11)
         }
    }, {underscored: true});

    return Schedule;
}
