module.exports = (sequelize, Sequelize) => {
    var Class = sequelize.define("class", {
        id: {
            type: Sequelize.INTEGER(11),
            allowNull: false,
            autoIncrement: true,
            primaryKey: true
        },
        classname: {
            type: Sequelize.STRING
        }
    }, {underscored: true});

    return Class;
};

