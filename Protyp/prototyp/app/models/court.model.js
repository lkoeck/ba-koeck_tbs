module.exports = (sequelize, Sequelize) => {
    var Court = sequelize.define("court", {
        id: {
            type: Sequelize.INTEGER(11),
            allowNull: false,
            autoIncrement: true,
            primaryKey: true
        },
        courtname: {
            type: Sequelize.STRING
        }
    }, {underscored: true});

    return Court;
};

