module.exports = (sequelize, Sequelize) => {
    var Discipline = sequelize.define("discipline", {
        id: {
            type: Sequelize.INTEGER(11),
            allowNull: false,
            autoIncrement: true,
            primaryKey: true
        },
        discipline: {
            type: Sequelize.STRING
        }
    }, {underscored: true});

    return Discipline;
};

