module.exports = (sequelize, Sequelize) => {
    const Registration = sequelize.define("registration", {
        id: {
            type: Sequelize.INTEGER(11),
            allowNull: false,
            autoIncrement: true,
            primaryKey: true
        },
        status:{
            type: Sequelize.INTEGER(11),
        },
        /*user_id: {
            //FK in User table
            type: Sequelize.INTEGER(11),
        },*/
        categoryId: {
            //FK in Category table
            type: Sequelize.INTEGER(11),
        }
    }, {underscored: true});

    return Registration;
}
