const dbConfig = require("../config/db.config.js");

// Init DB
const Sequelize = require("sequelize");
const sequelize = new Sequelize(dbConfig.DB, dbConfig.USER, dbConfig.PASSWORD, {
    host: dbConfig.HOST,
    dialect: dbConfig.dialect,
    operatorsAliases: false,

    pool: {
        max: dbConfig.pool.max,
        min: dbConfig.pool.min,
        acquire: dbConfig.pool.acquire,
        idle: dbConfig.pool.idle
    }
});

const db = {};

db.Sequelize = Sequelize;
db.sequelize = sequelize;

// Import Models
db.roles = require("./role.model.js")(sequelize, Sequelize);
db.clubs = require("./club.model.js")(sequelize, Sequelize);
db.athletes = require("./athlete.model.js")(sequelize, Sequelize);
db.disciplines = require("./discipline.model")(sequelize, Sequelize);
db.classes = require("./class.model.js")(sequelize, Sequelize);
db.compositions = require("./composition.model.js")(sequelize, Sequelize);
db.categories = require("./category.model.js")(sequelize, Sequelize);
db.athleteRegistrations = require("./athleteregistration.model")(sequelize,Sequelize);
db.registrations = require("./registration.model.js")(sequelize, Sequelize);
db.courts = require("./court.model.js")(sequelize,Sequelize);
db.schedules = require("./schedule.model.js")(sequelize,Sequelize);
db.scores = require("./score.model")(sequelize,Sequelize);
db.referees = require("./referee.model")(sequelize,Sequelize);

// Associations for Models (Foreign Keys)
//--------------------------------------------------------------
// FK Athletes
db.clubs.hasMany(db.athletes, {as: "clubs",foreignKey: "clubId"});
db.athletes.belongsTo(db.clubs, {foreignKey: 'clubId', allowNull: "false"});

// FK Category

db.disciplines.hasMany(db.categories,  {foreignKey: "disciplineId"});
db.classes.hasMany(db.categories,  {foreignKey: "classId"});
db.compositions.hasMany(db.categories,  {foreignKey: "compositionId"});
db.categories.belongsTo(db.disciplines, {foreignKey: 'disciplineId', allowNull: "false"});
db.categories.belongsTo(db.classes, {foreignKey: 'classId', allowNull: "false"});
db.categories.belongsTo(db.compositions, {foreignKey: 'compositionId', allowNull: "false"});

// FK Registration
db.categories.hasMany(db.registrations, {foreignKey: "categoryId"});
db.registrations.belongsTo(db.categories, {foreignKey: "categoryId" , allowNull: "false"});
db.athletes.belongsToMany(db.registrations, {through: 'athletsRegistrations'});
db.registrations.belongsToMany(db.athletes, {through: 'athletesRegistrations'});


// FK Schedule
db.courts.hasMany(db.schedules, {foreignKey: "courtId"});
db.categories.hasMany(db.schedules, {foreignKey: "categoryId"});
db.schedules.belongsTo(db.courts, {foreignKey: "courtId" , allowNull: "false"});
db.schedules.belongsTo(db.categories, {foreignKey: "categoryId" , allowNull: "false"});

// FK Score 
db.registrations.hasMany(db.scores, {foreignKey: "registrationId"});
db.scores.belongsTo(db.registrations, {foreignKey: "registrationId"});

db.referees.hasMany(db.scores, {foreignKey: "refereeId"});
db.scores.belongsTo(db.referees, {foreignKey: "refereeId"});

db.scores.belongsTo(db.courts,{foreignKey: "courtId"});
db.courts.hasOne(db.scores,{foreignKey: "courtId"});

// FK Referees
db.roles.hasMany(db.referees, {as: "refrees", foreignKey: "roleId"});
db.referees.belongsTo(db.roles, { foreignKey: 'roleId', allowNull: "false"});

db.ROLE = ["referee","admin"];

module.exports = db;