module.exports = (sequelize, Sequelize) => {
    var Composition = sequelize.define("composition", {
        id: {
            type: Sequelize.INTEGER(11),
            allowNull: false,
            autoIncrement: true,
            primaryKey: true
        },
        composition: {
            type: Sequelize.STRING
        }
    }, {underscored: true});

    return Composition;
};

