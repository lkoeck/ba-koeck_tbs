module.exports = (sequelize, Sequelize) => {
    const Athlete_Registration = sequelize.define("athleteRegistration", {
        id: {
            type: Sequelize.INTEGER(11),
            allowNull: false,
            autoIncrement: true,
            primaryKey: true
        }
    }, {underscored: true});

    return Athlete_Registration;
}
