//import logo from './logo.svg';
import './App.css';

function App() {
  return (
    <div className="App">
      <header className="App-header">
        <h1>WELCOME to Yeomju - the Poomsae Scoring System</h1>
        <button>Get Data</button>
      </header>
    </div>
  );
}

export default App;
