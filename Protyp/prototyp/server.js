const express = require("express");
const bodyParser = require("body-parser");
const cors = require("cors");
const http = require("http");

const app = express();

//Websocket
const server = http.createServer(app);
const { Server } = require("socket.io");
const io = new Server(server, {
  cors: true, 
  origins: ['http://localhost:3000','http://jeomju.tbs'],
});

const db = require("./app/models");
db.sequelize.sync();
//initial();
// Drops Table each time building 
/* db.sequelize.sync({ force: true }).then(() => {
    console.log("Drop and re-sync db.");
    initial();
}); */


var corsOptions = {
    //origin: "http://localhost:8081"
    credentials: true,
     origin:  ['http://localhost:3000','http://jeomju.tbs'],
     preflightContinue: false,
     methods: ["GET", "POST", "PUT", "DELETE"],
};

app.use(cors(corsOptions));

// parse requests of content-type - application/json
app.use(bodyParser.json());

// parse requests of content-type - application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({ extended: true }));

// Connection for Websocket
let allSockets = [];

io.on('connection', socket => {
  console.log("Client connected");
  console.log(socket.handshake.query);
  allSockets.push(socket);

  // Join Room
  const court = socket.handshake.query.court
  socket.join(court);
  socket.emit();


  socket.on("disconnect", () => {
    console.log("Client disconnected");
    let i = allSockets.indexOf(socket);
    allSockets.splice(i, 1);
  });
});

app.io = io;

// simple route
app.get("/", (req, res) => {
    res.json({ message: "Welcome to Jeomju, the Poomsae Scoring System" });
});

app.get('/test', (req, res) => {
  res.sendFile(__dirname + '/index.html');
 });

const Role = db.roles;
function initial() {
    Role.create({
      id: 1,
      title: "referee"
    });
    Role.create({
      id: 2,
      title: "admin"
    });
  };
  

//import routes
require('./app/routes/auth.routes')(app);
require('./app/routes/referee.routes')(app);

require("./app/routes/athlete.routes")(app);
require("./app/routes/category.routes")(app);
require("./app/routes/class.routes")(app);
require("./app/routes/club.routes")(app);
require("./app/routes/composition.routes")(app);
require("./app/routes/court.routes")(app);
require("./app/routes/discipline.routes")(app);
require("./app/routes/registration.routes")(app);
require("./app/routes/role.routes")(app);
require("./app/routes/schedule.routes")(app);
require("./app/routes/score.routes")(app, io)


const PORT = process.env.PORT || 8080;
server.listen(PORT, () => {
  console.log(`Listening on port ${PORT}`);
  //initial();
}
);
