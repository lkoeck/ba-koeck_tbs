-- MySQL dump 10.13  Distrib 8.0.23, for macos10.15 (x86_64)
--
-- Host: localhost    Database: tbs
-- ------------------------------------------------------
-- Server version	8.0.23

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `athlete_registrations`
--

DROP TABLE IF EXISTS `athlete_registrations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `athlete_registrations` (
  `id` int NOT NULL AUTO_INCREMENT,
  `createdAt` datetime NOT NULL,
  `updatedAt` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `athlete_registrations`
--

LOCK TABLES `athlete_registrations` WRITE;
/*!40000 ALTER TABLE `athlete_registrations` DISABLE KEYS */;
/*!40000 ALTER TABLE `athlete_registrations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `athletes`
--

DROP TABLE IF EXISTS `athletes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `athletes` (
  `id` int NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `date_of_birth` datetime DEFAULT NULL,
  `graduation` varchar(255) DEFAULT NULL,
  `club_id` int DEFAULT NULL,
  `createdAt` datetime NOT NULL,
  `updatedAt` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `club_id` (`club_id`),
  CONSTRAINT `athletes_ibfk_1` FOREIGN KEY (`club_id`) REFERENCES `clubs` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=37 DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `athletes`
--

LOCK TABLES `athletes` WRITE;
/*!40000 ALTER TABLE `athletes` DISABLE KEYS */;
INSERT INTO `athletes` VALUES (1,'Leona Köck','1999-02-25 00:00:00','2. Dan',1,'2021-06-17 10:51:37','2021-06-17 10:51:37'),(2,'Daniel Raich','1992-03-27 00:00:00','1. Dan',1,'2021-06-17 10:51:37','2021-06-17 10:51:37'),(3,'Elias Huber','2003-06-15 00:00:00','1. Dan',1,'2021-06-17 10:51:37','2021-06-17 10:51:37'),(4,'Peter Huber','1969-09-04 00:00:00','1. Kup',1,'2021-06-17 10:51:37','2021-06-17 10:51:37'),(5,'Simon Skalet','2008-03-19 00:00:00','4. Kup',1,'2021-06-17 10:51:37','2021-06-17 10:51:37'),(6,'Nadja Molin-Pradel','1998-03-03 00:00:00','3. Dan',1,'2021-06-17 10:51:37','2021-06-17 10:51:37'),(7,'Simone Primetshofer','1998-01-21 00:00:00','1. Dan',6,'2021-06-17 10:51:37','2021-06-17 10:51:37'),(8,'Günter Primetshofer','1967-09-13 00:00:00','3. Dan',6,'2021-06-17 10:51:37','2021-06-17 10:51:37'),(9,'Primetshofer; Primetshofer','1967-09-13 00:00:00','3. Dan',6,'2021-06-17 10:51:37','2021-06-17 10:51:37'),(10,'Alessandro Matthä','2009-07-31 00:00:00','4. Kup',6,'2021-06-17 10:51:37','2021-06-17 10:51:37'),(11,'Alessia Matthä','2011-02-21 00:00:00','8. Kup',6,'2021-06-17 10:51:37','2021-06-17 10:51:37'),(12,'Lena-Marie Jussel','2007-07-13 00:00:00','4. Kup',6,'2021-06-17 10:51:37','2021-06-17 10:51:37'),(13,'Samuel Reutz','2011-05-07 00:00:00','8. Kup',2,'2021-06-17 10:51:37','2021-06-17 10:51:37'),(14,'Manuela Untersweg','1967-05-13 00:00:00','8. Kup',2,'2021-06-17 10:51:37','2021-06-17 10:51:37'),(15,'Wanda Krumböck','2008-01-15 00:00:00','1. Dan',2,'2021-06-17 10:51:37','2021-06-17 10:51:37'),(16,'Hannah Leitner','2004-04-28 00:00:00','2. Dan',2,'2021-06-17 10:51:37','2021-06-17 10:51:37'),(17,'Leonie Koll','2002-11-17 00:00:00','1. Dan',2,'2021-06-17 10:51:37','2021-06-17 10:51:37'),(18,'Stephanie Wintner','2002-03-29 00:00:00','2. Dan',2,'2021-06-17 10:51:37','2021-06-17 10:51:37'),(19,'Andreas Brückl','1998-11-05 00:00:00','3. Dan',2,'2021-06-17 10:51:37','2021-06-17 10:51:37'),(20,'Leni Niedermayr','1959-01-31 00:00:00','4. Dan',2,'2021-06-17 10:51:37','2021-06-17 10:51:37'),(21,'Koll; Wintner; Niedermayr','1996-01-01 00:00:00','3. Dan',4,'2021-06-17 10:51:37','2021-06-17 10:51:37'),(22,'Beck Aimee','2006-01-15 00:00:00','1. Dan',4,'2021-06-17 10:51:37','2021-06-17 10:51:37'),(23,'Beck Aimee; Hilbrand Joel','2006-01-15 00:00:00','1. Dan',4,'2021-06-17 10:51:37','2021-06-17 10:51:37'),(24,'Beck Aimee; Skalet Elisa; Rahimi Samira','2006-01-15 00:00:00','1. Dan',4,'2021-06-17 10:51:37','2021-06-17 10:51:37'),(25,'Benchakri Reem','2012-05-23 00:00:00','10. Kup',4,'2021-06-17 10:51:37','2021-06-17 10:51:37'),(26,'Bertschler Anouk','2007-07-25 00:00:00','1. Kup',4,'2021-06-17 10:51:37','2021-06-17 10:51:37'),(27,'Doppler Lea','2012-06-11 00:00:00','5. Kup',4,'2021-06-17 10:51:37','2021-06-17 10:51:37'),(28,'Doppler Mia','2008-02-02 00:00:00','3. Kup',4,'2021-06-17 10:51:38','2021-06-17 10:51:38'),(29,'Hepp Eve; Beck Hannah; Hepp Natasha','1972-10-12 00:00:00','3. Dan',4,'2021-06-17 10:51:38','2021-06-17 10:51:38'),(30,'Hepp Natasha','2002-02-06 00:00:00','1. Dan',7,'2021-06-17 10:51:38','2021-06-17 10:51:38'),(31,'Hilbrand Joel','2006-09-23 00:00:00','1. Dan',7,'2021-06-17 10:51:38','2021-06-17 10:51:38'),(32,'Nesensohn Dennis','2003-03-20 00:00:00','1. Dan',7,'2021-06-17 10:51:38','2021-06-17 10:51:38'),(33,'Nesensohn Dennis; Hepp Natasha','2002-02-06 00:00:00','1. Dan',7,'2021-06-17 10:51:38','2021-06-17 10:51:38'),(34,'Ötztürk Hayriye','2001-02-07 00:00:00','8. Kup',7,'2021-06-17 10:51:38','2021-06-17 10:51:38'),(35,'Rahimi Samira','2006-12-17 00:00:00','1. Kup',7,'2021-06-17 10:51:38','2021-06-17 10:51:38'),(36,'Rahimi Samira; Hilbrand Joel; Beck Aimee; Skalet Elisa; Bertschler Anouk','2006-01-15 00:00:00','1. Dan',7,'2021-06-17 10:51:38','2021-06-17 10:51:38');
/*!40000 ALTER TABLE `athletes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `categories`
--

DROP TABLE IF EXISTS `categories`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `categories` (
  `id` int NOT NULL AUTO_INCREMENT,
  `category` varchar(255) DEFAULT NULL,
  `discipline_id` int DEFAULT NULL,
  `class_id` int DEFAULT NULL,
  `composition_id` int DEFAULT NULL,
  `createdAt` datetime NOT NULL,
  `updatedAt` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `discipline_id` (`discipline_id`),
  KEY `class_id` (`class_id`),
  KEY `composition_id` (`composition_id`),
  CONSTRAINT `categories_ibfk_1` FOREIGN KEY (`discipline_id`) REFERENCES `disciplines` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `categories_ibfk_2` FOREIGN KEY (`class_id`) REFERENCES `classes` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `categories_ibfk_3` FOREIGN KEY (`composition_id`) REFERENCES `compositions` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=70 DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `categories`
--

LOCK TABLES `categories` WRITE;
/*!40000 ALTER TABLE `categories` DISABLE KEYS */;
INSERT INTO `categories` VALUES (1,'Poomsae Individual',3,1,1,'2021-06-17 10:51:05','2021-06-17 10:51:05'),(2,'Poomsae Individual',3,1,2,'2021-06-17 10:51:05','2021-06-17 10:51:05'),(3,'Poomsae Individual',4,1,1,'2021-06-17 10:51:05','2021-06-17 10:51:05'),(4,'Poomsae Individual',4,1,2,'2021-06-17 10:51:05','2021-06-17 10:51:05'),(5,'Poomsae Individual',5,1,1,'2021-06-17 10:51:05','2021-06-17 10:51:05'),(6,'Poomsae Individual',5,1,2,'2021-06-17 10:51:05','2021-06-17 10:51:05'),(7,'Poomsae Individual',6,1,1,'2021-06-17 10:51:05','2021-06-17 10:51:05'),(8,'Poomsae Individual',6,1,2,'2021-06-17 10:51:05','2021-06-17 10:51:05'),(9,'Poomsae Individual',2,2,1,'2021-06-17 10:51:05','2021-06-17 10:51:05'),(10,'Poomsae Individual',2,2,2,'2021-06-17 10:51:05','2021-06-17 10:51:05'),(11,'Poomsae Individual',3,2,1,'2021-06-17 10:51:06','2021-06-17 10:51:06'),(12,'Poomsae Individual',3,2,2,'2021-06-17 10:51:06','2021-06-17 10:51:06'),(13,'Poomsae Individual',4,2,1,'2021-06-17 10:51:06','2021-06-17 10:51:06'),(14,'Poomsae Individual',4,2,2,'2021-06-17 10:51:06','2021-06-17 10:51:06'),(15,'Poomsae Individual',5,2,1,'2021-06-17 10:51:06','2021-06-17 10:51:06'),(16,'Poomsae Individual',5,2,2,'2021-06-17 10:51:06','2021-06-17 10:51:06'),(17,'Poomsae Individual',6,2,1,'2021-06-17 10:51:06','2021-06-17 10:51:06'),(18,'Poomsae Individual',6,2,2,'2021-06-17 10:51:06','2021-06-17 10:51:06'),(19,'Poomsae Individual',2,3,1,'2021-06-17 10:51:06','2021-06-17 10:51:06'),(20,'Poomsae Individual',2,3,2,'2021-06-17 10:51:06','2021-06-17 10:51:06'),(21,'Poomsae Individual',3,3,1,'2021-06-17 10:51:06','2021-06-17 10:51:06'),(22,'Poomsae Individual',3,3,2,'2021-06-17 10:51:06','2021-06-17 10:51:06'),(23,'Poomsae Individual',4,3,1,'2021-06-17 10:51:06','2021-06-17 10:51:06'),(24,'Poomsae Individual',4,3,2,'2021-06-17 10:51:06','2021-06-17 10:51:06'),(25,'Poomsae Individual',5,3,1,'2021-06-17 10:51:06','2021-06-17 10:51:06'),(26,'Poomsae Individual',5,3,2,'2021-06-17 10:51:06','2021-06-17 10:51:06'),(27,'Poomsae Individual',6,3,1,'2021-06-17 10:51:06','2021-06-17 10:51:06'),(28,'Poomsae Individual',6,3,2,'2021-06-17 10:51:06','2021-06-17 10:51:06'),(29,'Poomsae Pair',3,1,4,'2021-06-17 10:51:06','2021-06-17 10:51:06'),(30,'Poomsae Pair',4,1,4,'2021-06-17 10:51:06','2021-06-17 10:51:06'),(31,'Poomsae Pair',5,1,4,'2021-06-17 10:51:06','2021-06-17 10:51:06'),(32,'Poomsae Pair',6,1,4,'2021-06-17 10:51:06','2021-06-17 10:51:06'),(33,'Poomsae Pair',2,2,6,'2021-06-17 10:51:06','2021-06-17 10:51:06'),(34,'Poomsae Pair',3,2,6,'2021-06-17 10:51:06','2021-06-17 10:51:06'),(35,'Poomsae Pair',4,2,6,'2021-06-17 10:51:06','2021-06-17 10:51:06'),(36,'Poomsae Pair',5,2,6,'2021-06-17 10:51:06','2021-06-17 10:51:06'),(37,'Poomsae Pair',6,2,6,'2021-06-17 10:51:06','2021-06-17 10:51:06'),(38,'Poomsae Pair',2,3,6,'2021-06-17 10:51:06','2021-06-17 10:51:06'),(39,'Poomsae Pair',3,3,6,'2021-06-17 10:51:06','2021-06-17 10:51:06'),(40,'Poomsae Pair',4,3,6,'2021-06-17 10:51:06','2021-06-17 10:51:06'),(41,'Poomsae Pair',5,3,6,'2021-06-17 10:51:06','2021-06-17 10:51:06'),(42,'Poomsae Pair',6,3,6,'2021-06-17 10:51:06','2021-06-17 10:51:06'),(43,'Poomsae Team',3,1,7,'2021-06-17 10:51:07','2021-06-17 10:51:07'),(44,'Poomsae Team',3,1,8,'2021-06-17 10:51:07','2021-06-17 10:51:07'),(45,'Poomsae Team',4,1,7,'2021-06-17 10:51:07','2021-06-17 10:51:07'),(46,'Poomsae Team',4,1,8,'2021-06-17 10:51:07','2021-06-17 10:51:07'),(47,'Poomsae Team',5,1,7,'2021-06-17 10:51:07','2021-06-17 10:51:07'),(48,'Poomsae Team',5,1,8,'2021-06-17 10:51:07','2021-06-17 10:51:07'),(49,'Poomsae Team',6,1,7,'2021-06-17 10:51:07','2021-06-17 10:51:07'),(50,'Poomsae Team',6,1,8,'2021-06-17 10:51:07','2021-06-17 10:51:07'),(51,'Poomsae Team',2,2,6,'2021-06-17 10:51:07','2021-06-17 10:51:07'),(52,'Poomsae Team',3,2,6,'2021-06-17 10:51:07','2021-06-17 10:51:07'),(53,'Poomsae Team',4,2,6,'2021-06-17 10:51:07','2021-06-17 10:51:07'),(54,'Poomsae Team',5,2,6,'2021-06-17 10:51:07','2021-06-17 10:51:07'),(55,'Poomsae Team',6,2,6,'2021-06-17 10:51:07','2021-06-17 10:51:07'),(56,'Poomsae Team',2,3,6,'2021-06-17 10:51:07','2021-06-17 10:51:07'),(57,'Poomsae Team',3,3,6,'2021-06-17 10:51:07','2021-06-17 10:51:07'),(58,'Poomsae Team',4,3,6,'2021-06-17 10:51:07','2021-06-17 10:51:07'),(59,'Poomsae Team',5,3,6,'2021-06-17 10:51:07','2021-06-17 10:51:07'),(60,'Poomsae Team',6,3,6,'2021-06-17 10:51:07','2021-06-17 10:51:07'),(61,'Poomsae Team',6,3,6,'2021-06-17 10:51:07','2021-06-17 10:51:07');
/*!40000 ALTER TABLE `categories` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `classes`
--

DROP TABLE IF EXISTS `classes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `classes` (
  `id` int NOT NULL AUTO_INCREMENT,
  `classname` varchar(255) DEFAULT NULL,
  `createdAt` datetime NOT NULL,
  `updatedAt` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `classes`
--

LOCK TABLES `classes` WRITE;
/*!40000 ALTER TABLE `classes` DISABLE KEYS */;
INSERT INTO `classes` VALUES (1,'LK1','2021-06-17 10:50:50','2021-06-17 10:50:50'),(2,'LK2','2021-06-17 10:50:50','2021-06-17 10:50:50'),(3,'LK3','2021-06-17 10:50:51','2021-06-17 10:50:51');
/*!40000 ALTER TABLE `classes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `clubs`
--

DROP TABLE IF EXISTS `clubs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `clubs` (
  `id` int NOT NULL AUTO_INCREMENT,
  `clubname` varchar(255) DEFAULT NULL,
  `createdAt` datetime NOT NULL,
  `updatedAt` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `clubs`
--

LOCK TABLES `clubs` WRITE;
/*!40000 ALTER TABLE `clubs` DISABLE KEYS */;
INSERT INTO `clubs` VALUES (1,'TKD Nenzing','2021-06-17 10:49:07','2021-06-17 10:49:07'),(2,'TKD Mustang Nenzing','2021-06-17 10:49:08','2021-06-17 10:49:08'),(3,'TKD Mustang Ludesch','2021-06-17 10:49:08','2021-06-17 10:49:08'),(4,'TKD Mustang Feldkirch','2021-06-17 10:49:08','2021-06-17 10:49:08'),(5,'TKD Cobra Dornbirn','2021-06-17 10:49:08','2021-06-17 10:49:08'),(6,'TKD Dojang Bludenz','2021-06-17 10:49:08','2021-06-17 10:49:08'),(7,'TKD Montafon','2021-06-17 10:49:08','2021-06-17 10:49:08'),(8,'TKD Scoriopns','2021-06-17 10:49:08','2021-06-17 10:49:08');
/*!40000 ALTER TABLE `clubs` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `compositions`
--

DROP TABLE IF EXISTS `compositions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `compositions` (
  `id` int NOT NULL AUTO_INCREMENT,
  `composition` varchar(255) DEFAULT NULL,
  `createdAt` datetime NOT NULL,
  `updatedAt` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `compositions`
--

LOCK TABLES `compositions` WRITE;
/*!40000 ALTER TABLE `compositions` DISABLE KEYS */;
INSERT INTO `compositions` VALUES (1,'M','2021-06-17 10:50:36','2021-06-17 10:50:36'),(2,'W','2021-06-17 10:50:36','2021-06-17 10:50:36'),(3,'MM','2021-06-17 10:50:36','2021-06-17 10:50:36'),(4,'MW','2021-06-17 10:50:36','2021-06-17 10:50:36'),(5,'WW','2021-06-17 10:50:36','2021-06-17 10:50:36'),(6,'mixed','2021-06-17 10:50:36','2021-06-17 10:50:36'),(7,'MMM','2021-06-17 10:50:36','2021-06-17 10:50:36'),(8,'WWW','2021-06-17 10:50:36','2021-06-17 10:50:36'),(9,'MMMWW','2021-06-17 10:50:36','2021-06-17 10:50:36');
/*!40000 ALTER TABLE `compositions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `courts`
--

DROP TABLE IF EXISTS `courts`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `courts` (
  `id` int NOT NULL AUTO_INCREMENT,
  `courtname` varchar(255) DEFAULT NULL,
  `createdAt` datetime NOT NULL,
  `updatedAt` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `courts`
--

LOCK TABLES `courts` WRITE;
/*!40000 ALTER TABLE `courts` DISABLE KEYS */;
INSERT INTO `courts` VALUES (1,'Court 1','2021-06-17 10:51:21','2021-06-17 10:51:21'),(2,'Court 2','2021-06-17 10:51:21','2021-06-17 10:51:21'),(3,'Court 3','2021-06-17 10:51:21','2021-06-17 10:51:21');
/*!40000 ALTER TABLE `courts` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `disciplines`
--

DROP TABLE IF EXISTS `disciplines`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `disciplines` (
  `id` int NOT NULL AUTO_INCREMENT,
  `discipline` varchar(255) DEFAULT NULL,
  `createdAt` datetime NOT NULL,
  `updatedAt` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `disciplines`
--

LOCK TABLES `disciplines` WRITE;
/*!40000 ALTER TABLE `disciplines` DISABLE KEYS */;
INSERT INTO `disciplines` VALUES (1,'Bambini','2021-06-17 10:50:09','2021-06-17 10:50:09'),(2,'Sch�ler','2021-06-17 10:50:09','2021-06-17 10:50:09'),(3,'Kadetten','2021-06-17 10:50:09','2021-06-17 10:50:09'),(4,'Junioren','2021-06-17 10:50:09','2021-06-17 10:50:09'),(5,'Senioren 1','2021-06-17 10:50:09','2021-06-17 10:50:09'),(6,'Senioren 2','2021-06-17 10:50:09','2021-06-17 10:50:09'),(7,'Senioren 3','2021-06-17 10:50:09','2021-06-17 10:50:09'),(8,'under 18','2021-06-17 10:50:09','2021-06-17 10:50:09'),(9,'above 18','2021-06-17 10:50:09','2021-06-17 10:50:09');
/*!40000 ALTER TABLE `disciplines` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `referees`
--

DROP TABLE IF EXISTS `referees`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `referees` (
  `id` int NOT NULL AUTO_INCREMENT,
  `username` varchar(255) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `createdAt` datetime NOT NULL,
  `updatedAt` datetime NOT NULL,
  `role_id` int DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `role_id` (`role_id`),
  CONSTRAINT `referees_ibfk_1` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `referees`
--

LOCK TABLES `referees` WRITE;
/*!40000 ALTER TABLE `referees` DISABLE KEYS */;
INSERT INTO `referees` VALUES (1,'leona','$2a$08$mDR0GJA2t8jcrJCsyp1CLupUco73OkAkP.OIqvSCJ86T4Y4CgSbDS','2021-06-17 10:42:25','2021-06-17 10:42:25',1),(2,'tbs','$2a$08$8Go3QG8wE6ZImt7k4xP86OqfBNCQEhQ7hXTIN8Gi53.ydzXvwMeS2','2021-07-06 09:46:38','2021-07-06 09:46:39',2);
/*!40000 ALTER TABLE `referees` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `registrations`
--

DROP TABLE IF EXISTS `registrations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `registrations` (
  `id` int NOT NULL AUTO_INCREMENT,
  `status` int DEFAULT NULL,
  `category_id` int DEFAULT NULL,
  `createdAt` datetime NOT NULL,
  `updatedAt` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `category_id` (`category_id`),
  CONSTRAINT `registrations_ibfk_1` FOREIGN KEY (`category_id`) REFERENCES `categories` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `registrations`
--

LOCK TABLES `registrations` WRITE;
/*!40000 ALTER TABLE `registrations` DISABLE KEYS */;
INSERT INTO `registrations` VALUES (3,3,20,'2021-07-15 06:37:04','2021-07-22 11:54:34'),(4,3,20,'2021-07-15 06:37:10','2021-07-27 09:18:32'),(5,3,29,'2021-07-15 06:37:15','2021-07-28 07:07:41'),(6,1,29,'2021-07-15 06:37:16','2021-07-28 07:07:55'),(7,3,5,'2021-07-15 06:39:16','2021-07-27 09:25:29'),(8,3,5,'2021-07-15 06:40:40','2021-07-28 06:58:10'),(9,3,5,'2021-07-15 06:40:41','2021-07-28 07:01:10');
/*!40000 ALTER TABLE `registrations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `roles`
--

DROP TABLE IF EXISTS `roles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `roles` (
  `id` int NOT NULL AUTO_INCREMENT,
  `title` varchar(255) DEFAULT NULL,
  `createdAt` datetime NOT NULL,
  `updatedAt` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `roles`
--

LOCK TABLES `roles` WRITE;
/*!40000 ALTER TABLE `roles` DISABLE KEYS */;
INSERT INTO `roles` VALUES (1,'referee','2021-06-10 12:07:29','2021-06-10 12:07:29'),(2,'admin','2021-06-10 12:07:29','2021-06-10 12:07:29');
/*!40000 ALTER TABLE `roles` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `schedules`
--

DROP TABLE IF EXISTS `schedules`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `schedules` (
  `id` int NOT NULL AUTO_INCREMENT,
  `time` datetime DEFAULT NULL,
  `category_id` int DEFAULT NULL,
  `court_id` int DEFAULT NULL,
  `status` int DEFAULT NULL,
  `createdAt` datetime NOT NULL,
  `updatedAt` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `category_id` (`category_id`),
  KEY `court_id` (`court_id`),
  CONSTRAINT `schedules_ibfk_1` FOREIGN KEY (`category_id`) REFERENCES `categories` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `schedules_ibfk_2` FOREIGN KEY (`court_id`) REFERENCES `courts` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `schedules`
--

LOCK TABLES `schedules` WRITE;
/*!40000 ALTER TABLE `schedules` DISABLE KEYS */;
INSERT INTO `schedules` VALUES (1,'2020-08-25 04:00:00',20,1,2,'2021-07-14 17:44:35','2021-07-27 09:21:50'),(2,'2020-08-25 05:00:00',5,1,2,'2021-07-15 06:34:59','2021-07-28 07:02:56'),(3,'2020-08-25 06:00:00',29,1,1,'2021-07-15 06:35:43','2021-07-28 07:02:52');
/*!40000 ALTER TABLE `schedules` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `scores`
--

DROP TABLE IF EXISTS `scores`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `scores` (
  `id` int NOT NULL AUTO_INCREMENT,
  `registration_id` int DEFAULT NULL,
  `referee_id` int DEFAULT NULL,
  `court_id` int DEFAULT NULL,
  `round_id` int DEFAULT NULL,
  `technique` double DEFAULT NULL,
  `presentation` double DEFAULT NULL,
  `createdAt` datetime NOT NULL,
  `updatedAt` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `registration_id` (`registration_id`),
  KEY `referee_id` (`referee_id`),
  KEY `court_id` (`court_id`),
  CONSTRAINT `scores_ibfk_1` FOREIGN KEY (`registration_id`) REFERENCES `registrations` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `scores_ibfk_2` FOREIGN KEY (`referee_id`) REFERENCES `athletes` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `scores_ibfk_3` FOREIGN KEY (`court_id`) REFERENCES `courts` (`id`) ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=185 DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `scores`
--

LOCK TABLES `scores` WRITE;
/*!40000 ALTER TABLE `scores` DISABLE KEYS */;
INSERT INTO `scores` VALUES (169,4,1,1,1,2,3.6,'2021-07-27 09:18:08','2021-07-27 09:18:08'),(170,4,1,1,1,2,3.6,'2021-07-27 09:18:11','2021-07-27 09:18:11'),(171,4,1,1,1,2,3.6,'2021-07-27 09:18:13','2021-07-27 09:18:13'),(172,4,1,1,1,2,3.6,'2021-07-27 09:18:14','2021-07-27 09:18:14'),(173,4,1,1,1,2,3.6,'2021-07-27 09:18:15','2021-07-27 09:18:15'),(174,7,1,1,1,2,3.6,'2021-07-27 09:22:45','2021-07-27 09:22:45'),(175,7,1,1,1,2,3.6,'2021-07-27 09:22:49','2021-07-27 09:22:49'),(176,7,1,1,1,2,3.6,'2021-07-27 09:22:52','2021-07-27 09:22:52'),(177,7,1,1,1,2,3.6,'2021-07-27 09:23:28','2021-07-27 09:23:28'),(178,7,1,1,1,2,3.6,'2021-07-27 09:23:31','2021-07-27 09:23:31'),(179,8,1,1,1,2.1,2.1,'2021-07-28 06:57:29','2021-07-28 06:57:29'),(180,8,1,1,1,4,3,'2021-07-28 06:57:34','2021-07-28 06:57:34'),(181,8,1,1,1,4,3,'2021-07-28 06:57:39','2021-07-28 06:57:39'),(182,8,1,1,1,2.8,3,'2021-07-28 06:57:43','2021-07-28 06:57:43'),(183,8,1,1,1,2.5,3,'2021-07-28 06:57:53','2021-07-28 06:57:53'),(184,5,1,1,1,3.6,2.2,'2021-07-28 07:03:37','2021-07-28 07:03:37');
/*!40000 ALTER TABLE `scores` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user_registrations`
--

DROP TABLE IF EXISTS `user_registrations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `user_registrations` (
  `createdAt` datetime NOT NULL,
  `updatedAt` datetime NOT NULL,
  `athleteId` int NOT NULL,
  `registrationId` int NOT NULL,
  PRIMARY KEY (`athleteId`,`registrationId`),
  KEY `registrationId` (`registrationId`),
  CONSTRAINT `user_registrations_ibfk_1` FOREIGN KEY (`athleteId`) REFERENCES `athletes` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `user_registrations_ibfk_2` FOREIGN KEY (`registrationId`) REFERENCES `registrations` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user_registrations`
--

LOCK TABLES `user_registrations` WRITE;
/*!40000 ALTER TABLE `user_registrations` DISABLE KEYS */;
INSERT INTO `user_registrations` VALUES ('2021-07-15 06:42:13','2021-07-15 06:42:13',1,7),('2021-07-15 06:42:28','2021-07-15 06:42:28',6,8),('2021-07-15 06:42:38','2021-07-15 06:42:38',7,9),('2021-07-15 06:41:31','2021-07-15 06:41:31',10,3),('2021-07-15 06:41:38','2021-07-15 06:41:38',13,4),('2021-07-15 06:41:47','2021-07-15 06:41:47',22,5),('2021-07-15 06:41:52','2021-07-15 06:41:52',25,5),('2021-07-15 06:42:01','2021-07-15 06:42:01',26,6),('2021-07-15 06:42:06','2021-07-15 06:42:06',27,6);
/*!40000 ALTER TABLE `user_registrations` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2021-07-28 10:39:26
