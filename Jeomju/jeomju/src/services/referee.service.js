import axios from "axios";
import authHeader from "./auth-header";

//const API_URL = "http://localhost:80/api/";
const host = (process.env.NODE_ENV === "production") ? "https://jeomju.tbs" : "http://localhost:8080";

class RefereeService {
    getClasses(){
        return axios.get( host + "/api/classes", { headers: authHeader() });
    }

    postPoints(regId, roundId, teqPoints, prePoints, refereeId, courtId, refereeName ){
        return axios.post(host+"/api/scores",{
            registrationId: regId,
            roundId: roundId,
            technique: teqPoints,
            presentation: prePoints,
            refereeId: refereeId,
            refereeName: refereeName,
            courtId: courtId
        }, { headers: authHeader() });
    }

    getCategories(courtid){
        return axios.get( host+"/api/schedules/court/"+courtid, { headers: authHeader() })
    }

    getCurrentCategory(courtid){
        return axios.get( host + "/api/schedules/active/"+courtid, { headers: authHeader() });
    }

    getCategoryName(catid){
        return axios.get( host + "/api/categories/"+catid, { headers: authHeader() });   
    }

    getCurrentAthlete(catid){
        return axios.get( host + "/api/registrations/active/"+catid, { headers: authHeader() });   
    }

    getRegistrations(catid){
        return axios.get( host + "/api/registrations/categories/"+catid, { headers: authHeader() });   
    }
   
    getClubname(clubid){
        return axios.get( host + "/api/clubs/"+clubid, { headers: authHeader() });   
    }

    getScores(registrationId, roundId){
        return axios.get( host + "/api/scores/getAll"/*+registrationId+"/"+roundId*/,{params: {registrationId: registrationId, roundId:roundId}}, { headers: authHeader() });
    }

    updateScheduleStatus(scheduleId, status, courtId){
        return axios.put( host + "/api/schedules/"+scheduleId, {status: status, courtId: courtId}, { headers: authHeader() });
    }

    updateRegistrationStatus(catId, status, courtId){
        return axios.put( host + "/api/registrations/"+catId, {status: status, courtId: courtId}, { headers: authHeader() });
    }

   
}

export default new RefereeService();