import axios from "axios";
//const API_URL = "http://localhost:8080/api/";
const host = (process.env.NODE_ENV === "production") ? "https://jeomju.tbs" : "http://localhost:8080";

class AuthService {
  login(username, password) {
    return axios
      .post(host + "/api/auth/signin", { username, password })
      .then((response) => {
        if (response.data.accessToken) {
          sessionStorage.setItem("user", JSON.stringify(response.data));
        }

        return response.data;
      });
  }

  logout() {
    sessionStorage.removeItem("user");
  }

  register(username, password) {
    return axios.post(host + "/api/auth/signup", {
      username,
      password,
    });
  }
}

export default new AuthService();