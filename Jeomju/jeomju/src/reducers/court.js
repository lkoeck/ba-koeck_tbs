import {  SET_COURT, CLEAR_COURT, } from "../actions/types";
const initialState = {
    court: 1,
};

export default function CourtReducer (state = initialState, action){
    const { type } = action;

    switch(type){
        case SET_COURT:
           return { 
               ...state,
               court: action.court 
            } ;
        case CLEAR_COURT:
           return { 
               ...state,
               court: 1
            } ;
        default: 
            return  state 
    }
};
