import { 
    ADD_SCORE,
    CLEAR_SCORE
 } from "../actions/types";

const initialState = {
    score: []
};

export default function ScoreReducer(state = initialState , action) {
    const { type } = action;

    switch(type){
        case ADD_SCORE:
            return { 
                ...state,
                score: [...state.score, action.score] 
            } ;
        case CLEAR_SCORE:
            return { 
                ...state,
                score: []
            } ;
        default: 
            return state;
    }
};