import { 
    SET_TECHNIQUEPOINTS,
    SET_P_POWER,
    SET_P_RHYTMUS,
    SET_P_ENERGY,
    GET_TECHNIQUEPOINTS,
    GET_PRESENTATIONPOINTS,
    CLEAR_POINTS
 } from "../actions/types";

const initialState = {
    technique: 4,
    power: 1,
    rhytmus: 1,
    energy: 1,
};

export default function PointsReducer(state = initialState , action) {
    const { type } = action;

    switch(type){
        case SET_P_POWER:
            return { 
                ...state,
                power: action.power 
            } ;
        case SET_P_RHYTMUS:
            return { 
                ...state,
                rhytmus: action.rhytmus 
            } ;
        case SET_P_ENERGY:
            return { 
                ...state,
                energy: action.energy 
            } ;
        case SET_TECHNIQUEPOINTS:
            return {
                ...state,
                technique: action.technique
            } ;
        case GET_TECHNIQUEPOINTS: {
            return {
                techniquePoints: state.technique
            }
        };
        case GET_PRESENTATIONPOINTS: {
            return {
                presentationPoints: state.power + state.rhytmus + state.energy
            };
        };
        case CLEAR_POINTS:
            return {
                ...state,
                technique: 4,
                power: 1,
                rhytmus: 1,
                energy: 1,
            } ;  
        default: 
            return state;
    }
};