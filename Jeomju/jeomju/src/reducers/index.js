import { combineReducers } from "redux";
import auth from "./auth";
import message from "./message";
import PointsReducer from './points';
import FSPointsReducer from './fs.points';
import CourtReducer from './court';
import RegistrationReducer from './registration';
import ScoreReducer from './score';

export default combineReducers({
  auth: auth,
  message: message,
  points: PointsReducer,
  fsPoints: FSPointsReducer,
  court: CourtReducer,
  registration: RegistrationReducer,
  score: ScoreReducer,
});