import { 
    SET_REG_ID,
    SET_ROUND_ID,
    SET_CATEGORY,
    SET_CATEGORY_ID,
    CLEAR_CATEGORY,
 } from "../actions/types";

const initialState = {
    id: 0,
    round: "/",
    categoryId: null,
    category: "No Category asigned",
};

export default function RegistrationReducer(state = initialState , action) {
    const { type } = action;

    switch(type){
        case SET_REG_ID:
            return { 
                ...state,
                id: action.id 
            } ;
        case SET_ROUND_ID:
            return { 
                ...state,
                round: action.round 
            } ;
        case SET_CATEGORY:
            return { 
                ...state,
                category: action.category 
            } ;
        case SET_CATEGORY_ID :
            return {
                ...state, 
                categoryId :action.categoryId
            };
        case CLEAR_CATEGORY:
            return {
                ...state,
                category: "No Category asigned"
            } ;
        default: 
            return state;
    }
};