import { 
    SET_HEIGHT,
    SET_NUMBER,
    SET_GRADIENT,
    SET_SPARRING,
    SET_ACROBATIC,
    SET_MOVEMENT,
    SET_CREATIVITY,
    SET_HARMONY,
    SET_ENERGY,
    SET_MUSIC,
    SET_DEDUCTION,
    CLEAR_FSPOINTS
 } from "../actions/types";

const initialState = {
    height: 0,
    number: 0,
    gradient: 0,
    sparring: 0,
    acrobatic: 0,
    movement: 0,
    creativity: 0,
    harmony: 0,
    energy: 0,
    music: 0,
    deduction:0,
};

export default function FSPointsReducer(state = initialState , action) {
    const { type } = action;

    switch(type){
        case SET_HEIGHT:
            return { 
                ...state,
                height: action.height 
            } ;
        case SET_NUMBER:
            return { 
                ...state,
                number: action.number 
            } ;
        case SET_GRADIENT:
            return { 
                ...state,
                gradient: action.gradient 
            } ;
        case SET_SPARRING:
            return {
                ...state,
                sparring: action.sparring
            } ;
        case SET_ACROBATIC:
            return {
                ...state,
                acrobatic: action.acrobatic
            } ;
        case SET_MOVEMENT:
            return {
                ...state,
                movement: action.movement
            } ;
        case SET_CREATIVITY:
            return {
                ...state,
                creativity: action.creativity
            } ;
        case SET_HARMONY:
            return {
                ...state,
                harmony: action.harmony
            } ;
        case SET_ENERGY:
            return {
                ...state,
                energy: action.energy
            } ;
        case SET_MUSIC:
            return {
                ...state,
                music: action.music
            } ;
        case SET_DEDUCTION:
            return {
                ...state,
                deduction: action.deduction
            } ;
        case CLEAR_FSPOINTS:
            return {
                ...state,
                height: 0,
                number: 0,
                gradient: 0,
                sparring: 0,
                acrobatic: 0,
                movement: 0,
                creativity: 0,
                harmony: 0,
                energy: 0,
                music: 0,
                deduction:0,
            } ;  
        default: 
            return state;
    }
};