import { 
    SET_COURT,
    CLEAR_COURT,
    } from "./types";
  
  export const setCourt = (court) => ({
    type: SET_COURT,
    court: court,
  });
  
  export const clearCourt = () => ({
    type: CLEAR_COURT,
    court: 0,
  });
  
  
  export default {
    setCourt,
    clearCourt
  }