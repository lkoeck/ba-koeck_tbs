import { 
  SET_HEIGHT,
  SET_NUMBER,
  SET_GRADIENT,
  SET_SPARRING,
  SET_ACROBATIC,
  SET_MOVEMENT,
  SET_CREATIVITY,
  SET_HARMONY,
  SET_ENERGY,
  SET_MUSIC,
  SET_DEDUCTION,
  CLEAR_FSPOINTS
} from "../actions/types";

export const setFSHeight = (height) => ({
  type: SET_HEIGHT,
  height: height,
});


export const setFSNumber = (number) => ({
    type: SET_NUMBER,
    number: number,
});

export const setFSGradient = (gradient) => ({
    type: SET_GRADIENT,
    gradient: gradient,
});
export const setFSSparring = (sparring) => ({
  type: SET_SPARRING,
  sparring: sparring,
});
export const setFSAcrobatic = (acrobatic) => ({
  type: SET_ACROBATIC,
  acrobatic: acrobatic,
});
export const setFSMovement = (movement) => ({
  type: SET_MOVEMENT,
  movement: movement,
});
export const setFSCreativity = (creativity) => ({
  type: SET_CREATIVITY,
  creativity: creativity,
});
export const setFSHarmony = (harmony) => ({
  type: SET_HARMONY,
  harmony: harmony,
});
export const setFSEnergy = (energy) => ({
  type: SET_ENERGY,
  energy: energy,
});

export const setFSMusic = (music) => ({
    type: SET_MUSIC,
    music: music,
});

export const setFSDeduction = (deduction) => ({
  type: SET_DEDUCTION,
  deduction: deduction,
});

export const clearFSPoints = () => ({
    type: CLEAR_FSPOINTS,
    technique: 4,
    power: 1,
    rhytmus: 1,
    energy: 1,
});

export default {
  setFSHeight,
  setFSNumber,
  setFSGradient,
  setFSSparring,
  setFSAcrobatic,
  setFSMovement,
  setFSCreativity,
  setFSHarmony,
  setFSEnergy,
  setFSMusic,
  setFSDeduction,
  clearFSPoints
}