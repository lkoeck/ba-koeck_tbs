import { 
  SET_TECHNIQUEPOINTS, 
  SET_P_POWER,
  SET_P_RHYTMUS,
  SET_P_ENERGY,
  GET_TECHNIQUEPOINTS,
  GET_PRESENTATIONPOINTS,
  CLEAR_POINTS,
  } from "./types";

export const setTechniquePoints = (technique) => ({
  type: SET_TECHNIQUEPOINTS,
  technique: technique,
});

export const setPoomsaePresPower = (power) => ({
    type: SET_P_POWER,
    power: power,
});

export const setPoomsaePresRhytmus = (rhytmus) => ({
    type: SET_P_RHYTMUS,
    rhytmus: rhytmus,
});

export const setPoomsaePresEnergy = (energy) => ({
    type: SET_P_ENERGY,
    energy: energy,
});

export const getPoomsaeTechniquePoints = () => ({
  type: GET_TECHNIQUEPOINTS
})

export const getPoomsaePresentationPoints = () => ({
  type: GET_PRESENTATIONPOINTS
})

export const clearPoints = () => ({
    type: CLEAR_POINTS
});

export default {
  setTechniquePoints,
  setPoomsaePresPower,
  setPoomsaePresRhytmus,
  setPoomsaePresEnergy,
  getPoomsaeTechniquePoints,
  getPoomsaePresentationPoints,
  clearPoints
}