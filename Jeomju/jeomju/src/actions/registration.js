import { 
    SET_REG_ID,
    SET_ROUND_ID,
    SET_CATEGORY,
    SET_CATEGORY_ID,
    CLEAR_CATEGORY,
 } from "./types";
  
  export const setRegistrationId = (id) => ({
    type: SET_REG_ID,
    id: id,
  });
  
  export const setRoundId = (round) => ({
      type: SET_ROUND_ID,
      round: round,
  });
  
  export const setCategory = (category) => ({
      type: SET_CATEGORY,
      category: category,
  });

  export const setCategoryId = (categoryId) => ({
    type: SET_CATEGORY_ID,
    categoryId: categoryId,
});

  export const clearCategory = () => ({
    type: CLEAR_CATEGORY,
  })
  
  export default {
    setRegistrationId,
    setRoundId,
    setCategory,
    setCategoryId,
    clearCategory,
  }