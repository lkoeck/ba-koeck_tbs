import { 
    ADD_SCORE,
    CLEAR_SCORE
    } from "./types";
  
  export const addScore = (score) => ({
    type: ADD_SCORE,
    score: score,
  });
  
  export const clearScore = () => ({
    type: CLEAR_SCORE,
    score: [],
  });
  
  export default {
    addScore,
    clearScore
  }