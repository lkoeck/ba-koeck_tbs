import { createMuiTheme } from "@material-ui/core/styles";
const textDark = "#222222";
const accentColour = "#DFFF00";
const textColor = "#E5E5E5";
const textDisabled = "#888888";
const buttonHovered =  "#e7ff40";
 /* --BackgroundLayer3: #5B5B5B;
  --BackgroundLayer2: #3B3B3B;
  --BackgroundLayer1: #222222;
  --BackgroundLayer0: #121212;
  --Gray1: #9e9e9e;
  --Gray2: #4a4a4a;
  --Gray3: #2b2b2b;
  --Gray4: #1a1a1a; */

export default createMuiTheme({
    overrides: {
      //Button
      MuiButton: {
        root: {
          color: textDark,
        },
        containedPrimary: {
            backgroundColor: accentColour,
            color: textDark,
            "&:hover": {
                backgroundColor: buttonHovered,
              },
            "&:disabled": {
                color: textDisabled,
            },
        },
      },
      //Divider
      MuiDivider: {
        root: {
            backgroundColor: textDisabled,
        }
      },
      //Select
      MuiSelect: {
        root: {
            color: textColor,
            borderColor: "#E5E5E5 !important",  
        },
        select: {
            '&:focused': {
                borderColor: textColor,
              }
        },
        outlined: {
            '&:focused': {
                borderColor: textColor,
              } 
        }
      },
      MuiFormLabel:{
        root: {
            color: textColor,
            "&.Mui-focused": {
                color: textColor
              }
        }
      },
      MuiOutlinedInput: {
        root: {
            color: textColor,
            '&.Mui-focused fieldset': {
                borderColor: "#E5E5E5 !important"
            }
        } 
      },
      //Slider
      MuiSlider: {
        root: {
            color: accentColour,
        },
        thumb: {
            color: accentColour,
            backgroundColor: accentColour,
        },  
        rail: {
            opacity: 0.5,
            backgroundColor: '#bfbfbf',
        },
        valueLabel:{
            '& *': {
                backgroundColor: accentColour,
                color: textDark,
              },
        },
        markActive: {
            opacity: 1,
            backgroundColor: textDark,
          },
      }
    }
  });