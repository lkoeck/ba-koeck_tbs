import React from "react";
import JeomjuLogo from '../tbs_logo.svg';

export default function Home(props) {
  return (
    <div className="container">
      <div className="jumbotron box">
      <img src={JeomjuLogo} alt="Logo Jeomju" ></img>
      <h1>WELCOME to Jeomju - the Poomsae Scoring System</h1>
     </div>
    </div>
  );

}