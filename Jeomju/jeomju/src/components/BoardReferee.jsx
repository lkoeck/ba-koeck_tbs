import React from "react";
//import { useEffect, useState } from "react";
import { useHistory } from "react-router-dom";
import { useSelector } from 'react-redux';
import Button from '@material-ui/core/Button';
import FormControl from '@material-ui/core/FormControl';
import InputLabel from '@material-ui/core/InputLabel';
import Select from '@material-ui/core/Select';
//import UserService from "../services/user.service";

import './refereeComponents/refereeComponents.css';
import store from '../store';
import { setCourt } from '../actions/court';

export default function BoardReferee(props) {

  const history = useHistory();
  const courtId = useSelector((state) => state.court.court);

  const onChange = (event) => {
    store.dispatch(setCourt(event.target.value));
  };
  const onClick = () => {
    history.push("/currentAthlete");
  };

  return (
    <div className="container">
      <div className="jumbotron box">
        {/* <h3>{content}</h3> */}
        <h2>Hello Referee please select your court!</h2>
        <div className="selectCourtReferee">
          <FormControl variant="outlined">
            <InputLabel htmlFor="outlined-age-native-simple">Court</InputLabel>
            <Select
              native
              value={courtId}
              onChange={onChange}
              label="Court"
              inputProps={{
                name: 'court_id',
                id: 'outlined-age-native-simple',
              }}
            >
              <option aria-label="None" value="" />
              <option value={1}>Court 1</option>
              <option value={2}>Court 2</option>
              <option value={3}>Court 3</option>
            </Select>
          </FormControl>
          <Button variant="contained" color="primary" onClick={onClick}>
            Start to assess
          </Button>
        </div>
      </div>
    </div>
  );

}