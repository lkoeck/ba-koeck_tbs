import React from "react";
//import { useEffect, useState } from "react";

import './scoreComponents/scoreComponents.css'

import { useHistory } from "react-router-dom";
import store from '../store';
import Button from '@material-ui/core/Button';
import { setCourt } from '../actions/court';

export default function BoardScores(props) {

  const history = useHistory();
  //const courtId = useSelector((state) => state.court.court);

  const onClickCourtOne = (event) => {
    store.dispatch(setCourt(1));
    history.push("/scoreOverview");
  };
  const onClickCourtTwo = (event) => {
    store.dispatch(setCourt(2));
    history.push("/scoreOverview");
  };
  const onClickCourtThree = (event) => {
    store.dispatch(setCourt(3));
    history.push("/scoreOverview");
  };

  return (
    <div className="container">
      <div className="jumbotron box">
        <h2>Hello, please select your court!</h2>
        <div className="selectCourtScores">
          <Button variant="contained" color="primary" onClick={onClickCourtOne}>
            Court 1
          </Button>
          <Button variant="contained" color="primary" onClick={onClickCourtTwo}>
            Court 2
          </Button>
          <Button variant="contained" color="primary" onClick={onClickCourtThree}>
            Court 3
          </Button>
        </div>
      </div>
    </div>
  );
}
