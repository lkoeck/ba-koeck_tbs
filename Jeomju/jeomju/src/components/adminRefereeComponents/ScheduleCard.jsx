import React, { useEffect, useState } from "react";
import { useSelector } from 'react-redux';
import Button from '@material-ui/core/Button';

import RefereeService from '../../services/referee.service';
import RegistrationCard from './RegistrationCard';

export default function ScheduleCard(props) {

  const courtId = useSelector((state) => state.court.court);
  const categoryStatus = props.status;
  const categoryId = props.categoryId;
  const [categoryName, setCategoryName] = useState("CategoryName");
  const [ isFreestyle, setIsFreestyle] = useState(false);
  const [registrations, setRegistrations] = useState([]);

  const onClick = () => {
    RefereeService.updateScheduleStatus(props.id, categoryStatus + 1, courtId).then(res => {
      //console.log(res);
      if (res.status === 200) {
        props.updateData();
      }
    });
  }

  useEffect(() => {
    //console.log(props.scores);
    //Retrieve All Categories
    RefereeService.getCategoryName(categoryId).then(
      response => {
        console.log(response.data);
        let categoryName = response.data[0].category + " " + response.data[0].class.classname + " " + response.data[0].class.discipline + " " + response.data[0].class.composition
        setCategoryName(categoryName);
        if( categoryName.includes('Freestyle')){
          setIsFreestyle(true);
        }
      });

    RefereeService.getRegistrations(categoryId).then(
      response => {
        //console.log("registrations:");
        //console.log(response.data);
        setRegistrations(response.data);
      },
      error => {
        setRegistrations((error.response &&
          error.response.data &&
          error.response.data.message) ||
          error.message ||
          error.toString())
      }
    );
  }, [categoryStatus]);

  return (
    <div className="scheduleCard">
      <div>
   {/*      <h2>Category Name {props.id}</h2> */}
        <h3>{categoryName}</h3>
        <h3>Status: {(categoryStatus === 0) ? "on hold" : ((categoryStatus === 1) ? "active" : "finished" )}</h3>
        <Button variant="contained" color="primary" onClick={onClick} disabled={(categoryStatus >= 2) ? true : false}>
          Next Category
        </Button>
      </div>
      {
        registrations.map(r => (<RegistrationCard key={r.id} id={r.id} status={r.status} athletes={r.athletes}  scores={(r.status !== 0 && r.status < 3)? props.scores : []} disabled={(categoryStatus >= 2) ? true : false} isFreestyle={isFreestyle}></RegistrationCard>))
      }
    </div>);
}
