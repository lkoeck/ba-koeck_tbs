import React, { useState } from "react";
import Button from '@material-ui/core/Button';
import { useSelector } from 'react-redux';
import store from '../../store';
import { clearScore } from '../../actions/score';

import RefereeService from '../../services/referee.service';

export default function RegistrationCard(props) {

  const athletes = props.athletes;
  const courtId = useSelector((state) => state.court.court);
  const [disabled, setDisable] = useState(props.disabled);
  const [status, setStatus] = useState(props.status);
  const score = useSelector((state) => state.score.score);

  const onClick = () => {
    console.log(courtId);
    RefereeService.updateRegistrationStatus(props.id, (props.isFreestyle && status === 1) ? status+2 : status + 1, courtId ).then(res => {
      if (res.status === 200) {
        store.dispatch(clearScore());
        setStatus(res.data.updatedStatus);
        if (res.data.updatedStatus >= 3) {
          setDisable(true);
        }
      }
    })
  }

  return (
    <div className="registrationCard">
      <div>
        {
          athletes.map(a => (<h4 key={a.name}>{a.name}</h4>))
        }
      </div>
      {/* <h3>Registration ID: {props.id}</h3> */}
      <h4>{(status === 0) ? "Status: on hold" : ((status === 1) ? "Round 1" : (status === 2) ? "Round 2" : "Status: finished" )}</h4>
      <h4>Scores: {(score.length !== 0) ? ((score[0].registrationId === props.id) ? score.length : "0") : "0"}</h4>
      {/*  <h4>Counter: {childCounter}</h4> */}
      <Button variant="contained" color="primary" onClick={onClick} disabled={(status >= 3) || (disabled) ? true : false}>
        Next Status
      </Button>
    </div>
  );
}
