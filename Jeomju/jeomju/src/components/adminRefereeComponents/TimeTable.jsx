import React, { useEffect, useState } from "react";
import { io } from "socket.io-client";
import { useSelector } from 'react-redux';
import store from '../../store';

import RefereeService from "../../services/referee.service";
import ScheduleCard from './ScheduleCard';
import { addScore } from '../../actions/score';

export default function TimeTable() {

    const courtId = useSelector((state) => state.court.court);
    const [response, setResponse] = useState([]);
    const [schedules, setSchedules] = useState([]);
    const [toggleReload, toggle] = useState(true);

    const updateData = () => {
        toggle(!toggleReload);
    }

    useEffect(() => {
        //Retrieve All Categories
        RefereeService.getCategories(courtId).then(
            response => {
                //console.log("schedule:");
                //console.log(response.data);
                setSchedules(response.data);
            },
            error => {
                setSchedules((error.response &&
                    error.response.data &&
                    error.response.data.message) ||
                    error.message ||
                    error.toString())
            }
        );
    },[toggleReload]);

    useEffect(() => {
        const socket = io((process.env.NODE_ENV === "production") ? "https://jeomju.tbs" : "http://localhost:8080",{ //
          query: {
            court: "court"+courtId,
          },
        });
        socket.on("postedScore", data => {
          //console.log(socket.id);
          console.log(data);
          setResponse(response => [...response, data]);
          store.dispatch(addScore(data));
          //setTotalPoints(0);
          console.log(response);
        });
        socket.on("nextRegistration", () => {
            setResponse([]);
        });
        return () => socket.disconnect();
      }, []);

    return (
        <div>
            <h1>TimeTable of Court {courtId}</h1>
            {
                schedules.map(s => (<ScheduleCard key={s.categoryId} id={s.id} categoryId={s.categoryId} status={s.status} scores={response} updateData={updateData}></ScheduleCard>))
            }
        </div>
    );

}