import { useHistory } from "react-router-dom";
import { useSelector } from 'react-redux';
import Button from '@material-ui/core/Button';
import Slider from '@material-ui/core/Slider';

import store from '../../store';
import { setFSCreativity, setFSHarmony, setFSEnergy, setFSMusic, setFSDeduction, } from '../../actions/fs.points';

export default function FSPresentationPoints(props) {
    const history = useHistory();
    const FSPoints = useSelector((state) => state.fsPoints);
    const creativity = FSPoints.creativity;
    const deduction = FSPoints.deduction;
    const energy = FSPoints.energy;
    const harmony = FSPoints.harmony;
    const music = FSPoints.music;
    const totalpoints = Math.round((creativity + harmony + energy + music + deduction) * 10) / 10;

    const onClick = () => {
        history.push("/poomsae/overview");
    }
    const onCreativityChange = (event, newValue) => {
        store.dispatch(setFSCreativity(newValue));
    }
    const onHarmonyChange = (event, newValue) => {
        store.dispatch(setFSHarmony(newValue));
    }
    const onEnergyChange = (event, newValue) => {
        store.dispatch(setFSEnergy(newValue));
    }
    const onMusicChange = (event, newValue) => {
        store.dispatch(setFSMusic(newValue));
    }
    const onDeductionMinus = () => {
        if (deduction <= 0) {
            /* if ((deduction - 0.1) < 0) {
                store.dispatch(setFSDeduction(0));
                console.log("0 deduction");
            }
            else { */
                var newPoints = Math.round((deduction - 0.1) * 10) / 10;
                console.log(newPoints);
                store.dispatch(setFSDeduction(newPoints));
            //}
        }
    }
    const onDeductionPlus = () => {
        if ((deduction + 0.1) > 0) {
            store.dispatch(setFSDeduction(0));
            //console.log("0 deduction");
        }
        else { 
            var newPoints = Math.round((deduction + 0.1) * 10) / 10;
            store.dispatch(setFSDeduction(newPoints));
        }
  
    }

    return (
        <div>
            <h1 className="textHighlihted centerHeader">{totalpoints}</h1>
            <h4>Creativity</h4>
            <Slider
                value={creativity}
                aria-labelledby="discrete-slider-small-steps"
                step={0.1}
                marks
                min={0}
                max={1}
                valueLabelDisplay="auto"
                onChange={onCreativityChange}
            />
            <h4>Harmony</h4>
            <Slider
                value={harmony}
                aria-labelledby="discrete-slider-small-steps"
                step={0.1}
                marks
                min={0}
                max={1}
                valueLabelDisplay="auto"
                onChange={onHarmonyChange}
            />
            <h4>Expression of Energy</h4>
            <Slider
                value={energy}
                aria-labelledby="discrete-slider-small-steps"
                step={0.1}
                marks
                min={0}
                max={1}
                valueLabelDisplay="auto"
                onChange={onEnergyChange}
            />
            <h4>Music & Choregraphy</h4>
            <Slider
                value={music}
                aria-labelledby="discrete-slider-small-steps"
                step={0.1}
                marks
                min={0}
                max={1}
                valueLabelDisplay="auto"
                onChange={onMusicChange}
            />
            <h4>Deductions: {deduction}</h4>
            <div className="centerButton flexSpaceAround">
                <Button variant="contained" color="primary" onClick={onDeductionMinus}>
                    -0.1
                </Button>
                <Button variant="contained" color="primary" onClick={onDeductionPlus}>
                    +0.1
                </Button>
            </div>
            <div className="centerButton marginTop">
                <Button variant="contained" color="primary" onClick={onClick}>
                    Continue to Send Points
                </Button>
            </div>
        </div>
    )
}