import { useHistory } from "react-router-dom";
import { useSelector } from 'react-redux';
import Button from '@material-ui/core/Button';
import Slider from '@material-ui/core/Slider';

import store from '../../store';
import { setFSHeight, setFSNumber, setFSGradient, setFSSparring, setFSAcrobatic, setFSMovement, } from '../../actions/fs.points';

export default function FSTechniquePoints(props) {
    
    const history = useHistory();
    const FSPoints = useSelector((state) => state.fsPoints);
    const height = FSPoints.height;
    const number = FSPoints.number;
    const gradient = FSPoints.gradient;
    const sparring = FSPoints.sparring;
    const acrobatic = FSPoints.acrobatic;
    const movement = FSPoints.movement;

    const totalpoints = Math.round((height + number + gradient + sparring + acrobatic + movement) * 10) / 10;
    

    const onAcrobaticChange = (event, newValue) => {
        store.dispatch(setFSAcrobatic(newValue));
    }
    const onClick = () => {
        history.push("/freestyle/presentation");
    }
    const onGradientChange = (event, newValue) => {
        store.dispatch(setFSGradient(newValue));
    }
    const onHeightChange = (event, newValue) => {
        store.dispatch(setFSHeight(newValue));
    }
    const onNumberChange = (event, newValue) => {
        store.dispatch(setFSNumber( newValue));
    }
    const onMovementChange = (event, newValue) => {
        store.dispatch(setFSMovement(newValue));
    }
    const onSparringChange = (event, newValue) => {
        store.dispatch(setFSSparring(newValue));
    }

    return (
        <div>
            <h1 className="textHighlihted centerHeader">{totalpoints}</h1>
            <h4>Level of Difficulty of Foot Techniques</h4>
            <h5>Height of jumping Sidekick</h5>
            <Slider
                value={height}
                aria-labelledby="discrete-slider-small-steps"
                step={0.1}
                marks
                min={0}
                max={1}
                valueLabelDisplay="auto"
                onChange={onHeightChange}
            />
            <h5>Number of jumping front kicks in a jump</h5>
            <Slider
                value={number}
                aria-labelledby="discrete-slider-small-steps"
                step={0.1}
                marks
                min={0}
                max={1}
                valueLabelDisplay="auto"
                onChange={onNumberChange}
            />
            <h5>Gradient of Spins in a Spin Kick</h5>
            <Slider
                value={gradient}
                aria-labelledby="discrete-slider-small-steps"
                step={0.1}
                marks
                min={0}
                max={1}
                valueLabelDisplay="auto"
                onChange={onGradientChange}
            />
            <h5>Performance level of consecutive Sparring Kicks</h5>
            <Slider
                value={sparring}
                aria-labelledby="discrete-slider-small-steps"
                step={0.1}
                marks
                min={0}
                max={1}
                valueLabelDisplay="auto"
                onChange={onSparringChange}
            />
            <h5>Acrobatic actions</h5>
            <Slider
                value={acrobatic}
                aria-labelledby="discrete-slider-small-steps"
                step={0.1}
                marks
                min={0}
                max={1}
                valueLabelDisplay="auto"
                onChange={onAcrobaticChange}
            />
            <h5>Basic Movements and Practicability</h5>
            <Slider
                value={movement}
                aria-labelledby="discrete-slider-small-steps"
                step={0.1}
                marks
                min={0}
                max={1}
                valueLabelDisplay="auto"
                onChange={onMovementChange}
            />
            <div className="centerButton marginTop">
                <Button variant="contained" color="primary" onClick={onClick}>
                    Continue to Presentation Points
                </Button>
            </div>
        </div>
    )
}