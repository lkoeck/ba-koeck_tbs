import React, { useEffect, useState } from "react";
import { io } from "socket.io-client";
import { useHistory } from "react-router-dom";
import { useSelector } from 'react-redux';
import Button from '@material-ui/core/Button';

import { clearPoints } from '../../actions/points';
import store from '../../store';
import RefereeService from "../../services/referee.service";
import PointColumn from "../scoreComponents/PointColumn";

//import { getPoomsaeTechniquePoints, getPoomsaePresentationPoints,} from '../../actions/points';


export default function PoomsaeOverviewPoints(props) {

  const history = useHistory();
  const category = useSelector((state) => state.registration.category);
  const courtId = useSelector((state) => state.court.court);
  const [ isButtonDisabled, setButtonDisabled] = useState(false);
  const isFreestyle = (category.includes('Freestyle')) ? true : false;
  const [ scores, setScores] = useState([]);
  const [ totalScores, setTotalScores] = useState(0);
  const username = useSelector((state) => state.auth.user.username);

  //Poomsae Points from State
  const points = useSelector((state) => state.points);
  const totalPresentationPoints = Math.round((points.power + points.rhytmus + points.energy) * 10) / 10;
  const totalPoints = Math.round((points.technique + totalPresentationPoints) * 100) / 100;

  //FS Points from State
  //Technique
  const FSPoints = useSelector((state) => state.fsPoints);
  const fsTechniquePoints = Math.round((FSPoints.height + FSPoints.number + FSPoints.gradient + FSPoints.sparring + FSPoints.acrobatic + FSPoints.movement) * 10) / 10;
   //presentation
  const totalFsPresentationpoints = Math.round((FSPoints.creativity + FSPoints.harmony + FSPoints.energy + FSPoints.music + FSPoints.deduction) * 10) / 10;

  //Deductions
  //const totalTechPoints = Math.round((fsTechniquePoints - FSPoints.deduction) * 10) / 10;
  const totalFsPoints = Math.round((fsTechniquePoints + totalFsPresentationpoints) * 100) / 100;

  //Registration details
  const registration = useSelector((state) => state.registration);
  const userid = useSelector((state) => state.auth.user.id);

  useEffect(() => {
    const socket = io((process.env.NODE_ENV === "production") ? "https://jeomju.tbs" : "http://localhost:8080", { //
      query: {
        court: "court" + courtId,
      },
    });
    socket.on("changedRegistration", data => {
      if (data.status === 1 || data.status === 2) {
        store.dispatch(clearPoints());
        history.push("/currentAthlete");
      }
    });


    socket.on("5 Scores detected", () => {
      //Scores anzeigen
      RefereeService.getScores(parseInt(registration.id), parseInt(registration.round)).then(res => {
        console.log(res);
        let scoresArr = [];
        res.data.forEach(element => {
          scoresArr.push([element.referee.username, element.technique, element.presentation, Math.round((element.technique+element.presentation) * 100) / 100, false]);
        });

        //setScores(scoresArr);
        //alcTotalScore(res.data);

        let points = [];
        scoresArr.forEach(element => {
          points.push(element[3]);
          //points += element.technique + element.presentation;
        });
     
        let sort = points.slice().sort();
        let strike1 = points.indexOf(sort[0]);
        let strike2 = points.lastIndexOf(sort[4]);
    
        //points.sort(); //Array sortieren
        sort.shift(); // Erstes Element entfernen
        sort.pop(); //Letztes Element entfernen
        let value = sort.reduce((a, b) => a + b, 0)
        
        setTotalScores(Math.round((value/3)*100)/100);
      
          //let scoresCopy = scoresArr.slice();
          for( let i = 0; i < scoresArr.length; i++){
            if(i === strike1 || i === strike2){
              scoresArr[i][4] = true;
            }
          } 
          console.log(scoresArr);
          setScores(scoresArr);

      });

    });

    return () => socket.disconnect();

  }, []);


  const onClick = () => {
    //console.log(isFreestyle);
    //send points 
    if (isFreestyle) {
      RefereeService.postPoints(registration.id, registration.round, fsTechniquePoints, totalFsPresentationpoints, userid, parseInt(courtId), username);
    }
    else {
      RefereeService.postPoints(registration.id, registration.round, points.technique , totalPresentationPoints, userid, parseInt(courtId), username);
    }
    setButtonDisabled(true);
  }

 /*  const calcTotalScore = (scores) => {
    //Calculate Total Points of Scores
    let points = [];
    scores.forEach(element => {
      points.push(Math.round((element.technique + element.presentation) * 10) / 10);
      //points += element.technique + element.presentation;
    });
    points.sort(); //Array sortieren
    points.shift(); // Erstes Element entfernen
    points.pop(); //Letztes Element entfernen
    let value = points.reduce((a, b) => a + b, 0)
    console.log(Math.round(((value) / (scores.length - 2)) * 10) / 10);
    setTotalPoints(Math.round(((value) / (scores.length - 2)) * 10) / 10);
  } */

  return (
    <div>
      <div className="pointOverview">
        <div>
          <div>Technique Points: </div>
          <div>{isFreestyle ? fsTechniquePoints : points.technique }</div>
        </div>
        <div>
          <div>Presentation Points: </div>
          <div>{isFreestyle ? totalFsPresentationpoints : totalPresentationPoints}</div>
        </div>
        <div>
          <div>Total Points: </div>
          <div className="textHighlihted">{isFreestyle ? totalFsPoints : totalPoints}</div>
        </div>
      
        <div className="centerButton marginTop">
          <Button variant="contained" color="primary" onClick={onClick} disabled={isButtonDisabled}>
            Send Points
          </Button>
        </div>
      </div>
      <div className="scoresOverview scores">
      <div>
            <div>R</div>
            <div>T</div>
            <div>P</div>
            <div>∑</div>
          </div>
      {
       (scores.length !== 0) ? (scores.map(col => (<PointColumn key={scores.indexOf(col)} column={col} ></PointColumn>))) : ""
      }
        <div className="total">
          <h1 className="textHighlihted">{totalScores}</h1>
        </div>
      </div>
    </div>
  );
}