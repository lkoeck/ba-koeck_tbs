import { useHistory } from "react-router-dom";
import { useSelector} from 'react-redux';
import Button from '@material-ui/core/Button';

import {clearPoints} from '../../actions/points';
import store from '../../store';
import RefereeService from "../../services/referee.service";

export default function FSOverviewPoints(props) {
    //Points from State
    //Technique
    const FSPoints = useSelector((state) => state.fsPoints);
    const creativity = FSPoints.creativity;
    const harmony = FSPoints.harmony;
    const energy = FSPoints.energy;
    const music = FSPoints.music;
    const techniquepoints = Math.round((creativity+harmony+energy+music)*10)/10;
    //presentation
    const height = FSPoints.height;
    const number = FSPoints.number;
    const gradient = FSPoints.gradient;
    const sparring = FSPoints.sparring;
    const acrobatic = FSPoints.acrobatic;
    const movement = FSPoints.movement;
    const totalpresentationpoints = Math.round((height+number+gradient+sparring+acrobatic+movement)*10)/10;
    //Deductions
    const deduction = FSPoints.deduction;
    const totalpoints = Math.round((techniquepoints+totalpresentationpoints-deduction)*10)/10;
    const totaltechpoints = Math.round((techniquepoints-deduction)*10)/10;
    //Registration details
    const registration = useSelector((state) => state.registration); 
    const regid = registration.id;
    const roundid = registration.round;
    const userid =  useSelector((state) => state.auth.user.id); 

    const courtId = useSelector((state) => state.court.court);
    const history = useHistory();

    const onClick = () => {
      //send points 
      RefereeService.postPoints(regid,roundid,totaltechpoints,totalpresentationpoints,userid, parseInt(courtId));
      //if ok set points to default
      store.dispatch(clearPoints());
      history.push("/currentAthlete" );
    }

    return(
        <div>
            <h4>Technique Points:{techniquepoints}</h4>
            <h4>Presentation Points:{totalpresentationpoints}</h4>
            <h4>Deductions: -{deduction}</h4>
            <h4>Total:{totalpoints}</h4>
             <Button variant="contained" color="primary" onClick={onClick}>
                Send Points
             </Button>
        </div>
    )
}