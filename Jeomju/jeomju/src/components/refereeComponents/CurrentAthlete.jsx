import React, { useEffect, useState } from "react";
import { useHistory } from "react-router-dom";
import { useSelector } from 'react-redux';
import Button from '@material-ui/core/Button';

import store from '../../store';
import RefereeService from "../../services/referee.service";
import { setRegistrationId, setRoundId, setCategory, setCategoryId } from '../../actions/registration';

export default function CurrentAthlete() {


  const courtId = useSelector((state) => state.court.court);
  const [club, setClub] = useState("Club");
  const history = useHistory();
  const [isFreestyle, toggleIsFreestyle] = useState(false);
  const [name, setName] = useState([]);
  const registration = useSelector((state) => state.registration);


  const onClickToFreestyle = () => {
    history.push("/freestyle/technique");
  }
  const onClickToPoomsae = () => {
    history.push("/poomsae/technique");
  }

  useEffect(() => {
    //Retrieve Active Category
    RefereeService.getCurrentCategory(courtId).then(
      response => {
        let catid = response.data[0].categoryId;
        store.dispatch(setCategoryId(catid));
        //Get Name of Active Category
        RefereeService.getCategoryName(catid).then(
          response => {
            let cat = response.data[0].category + " " + response.data[0].class.classname + " " + response.data[0].class.discipline + " " + response.data[0].class.composition;
            store.dispatch(setCategory(cat));
            if (cat.includes('Freestyle')) {
              toggleIsFreestyle(true);
            }

            //Get Name of Current Athlete
            RefereeService.getCurrentAthlete(catid).then(
              response => {
                //console.log(response);
                if (response.data[0] !== undefined) {
                  let athletes = response.data[0].athletes;
                  athletes.forEach(athlete => {
                    setName(name => [...name, athlete.name]);
                  });
                  //setName(response.data[0].athletes[0].name);
                  let clubid = response.data[0].athletes[0].clubId;
                  store.dispatch(setRegistrationId(response.data[0].id));
                  store.dispatch(setRoundId(response.data[0].status));

                  //Get Clubname of the Current Athlete
                  RefereeService.getClubname(clubid).then(
                    response => {
                      setClub(response.data.clubname);
                    });
                }
              });
          });
      });
  }, [courtId]);

  return (
    <div>
      <h2>{registration.category}</h2>
      <div className="athleteInformation">
        {
          name.map(n => (<h3 key={n}>{n}</h3>))
        }
        <h3>{club}</h3>
        <h3>Durchgang: {registration.round}</h3>
      </div>
      <div className="centerButton flexSpaceAround">
      <Button variant="contained" color="primary" onClick={onClickToPoomsae} disabled={isFreestyle}>
        Continue to Scoring
      </Button>
      <Button variant="contained" color="primary" onClick={onClickToFreestyle} disabled={!isFreestyle}>
        Continue to FS Scoring
      </Button>
      </div>
    </div>
  );

}