import { useHistory } from "react-router-dom";
import { useSelector } from 'react-redux';
import Button from '@material-ui/core/Button';
import Slider from '@material-ui/core/Slider';

import store from '../../store';
import { setPoomsaePresPower, setPoomsaePresRhytmus, setPoomsaePresEnergy } from '../../actions/points';

export default function PoomsaePresentationPoints(props) {
    const history = useHistory();
    const Points = useSelector((state) => state.points);
    const energyPoints = Points.energy;
    const powerPoints = Points.power;
    const rhytmusPoints = Points.rhytmus;

    const totalpoints = Math.round((powerPoints + rhytmusPoints + energyPoints) * 10) / 10;

    const onClick = () => {
        history.push("/poomsae/overview");
    }
    const onEnergyChange = (event, newValue) => {
        store.dispatch(setPoomsaePresEnergy(newValue));
    }
    const onPowerChange = (event, newValue) => {
        store.dispatch(setPoomsaePresPower(newValue));
    }
    const onRhytmusChange = (event, newValue) => {
        store.dispatch(setPoomsaePresRhytmus(newValue));
    }

    return (
        <div>
            <h1 className="textHighlihted centerHeader">{totalpoints}</h1>
            <h4>Power & Speed</h4>
            <Slider
                value={powerPoints}
                aria-labelledby="discrete-slider-small-steps"
                step={0.1}
                marks
                min={0.5}
                max={2}
                valueLabelDisplay="auto"
                onChange={onPowerChange}
            />
            <h4>Rhytmus</h4>
            <Slider
                value={rhytmusPoints}
                aria-labelledby="discrete-slider-small-steps"
                step={0.1}
                marks
                min={0.5}
                max={2}
                valueLabelDisplay="auto"
                onChange={onRhytmusChange}
            />
            <h4>Expression of Energy</h4>
            <Slider
                value={energyPoints}
                aria-labelledby="discrete-slider-small-steps"
                step={0.1}
                marks
                min={0.5}
                max={2}
                valueLabelDisplay="auto"
                onChange={onEnergyChange}
            />
            <div className="centerButton">
                <Button variant="contained" color="primary" onClick={onClick}>
                    Continue to Send Points
                </Button>
            </div>
        </div >
    )
}