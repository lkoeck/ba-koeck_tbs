import { useHistory } from "react-router-dom";
import { useSelector } from 'react-redux';
import Button from '@material-ui/core/Button';

import store from '../../store';
import { setTechniquePoints } from '../../actions/points';

export default function PoomsaeTechniquePoints(props) {
    const history = useHistory();
    const techniquepoints = useSelector((state) => state.points.technique);


    const onBigMistake = () => {
        if (techniquepoints > 0) {
            if ((techniquepoints - 0.3) < 0) {
                store.dispatch(setTechniquePoints(0));
            }
            else {
                var newPoints = Math.round((techniquepoints - 0.3) * 10) / 10;
                store.dispatch(setTechniquePoints(newPoints));
            }
        }
    }
    const onClick = () => {
        history.push("/poomsae/presentation");
    }
    const onSmallMistake = () => {
        if (techniquepoints > 0) {
            if ((techniquepoints - 0.1) < 0) {
                store.dispatch(setTechniquePoints(0));
            }
            else {
                var newPoints = Math.round((techniquepoints - 0.1) * 10) / 10;
                store.dispatch(setTechniquePoints(newPoints));
            }
        }
    }

    return (
        <div>
            <h1 className="textHighlihted centerHeader">{techniquepoints}</h1>
            <div className="styledButtons">
                <Button variant="contained" color="primary" onClick={onBigMistake} style={{backgroundColor:'#9e9e9e', fontSize:'63px', width:'50%', height:'3em', marginRight:'20px'}}>
                    -0.3
                </Button>
                <Button variant="contained" color="primary" onClick={onSmallMistake} style={{backgroundColor:'#9e9e9e', fontSize:'63px', width:'50%', height:'3em'}}>
                    -0.1
                </Button>
            </div>
            <div className="centerButton">
                <Button variant="contained" color="primary" onClick={onClick}>
                    Continue to Presentation Points
                </Button>
            </div>
        </div>
    )
}
