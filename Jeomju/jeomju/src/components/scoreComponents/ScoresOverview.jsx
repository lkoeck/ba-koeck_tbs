import React, { useEffect, useState } from "react";
import { io } from "socket.io-client";
import { useHistory } from "react-router-dom";
import { useSelector } from 'react-redux';
import Button from '@material-ui/core/Button';
import Divider from '@material-ui/core/Divider';

//import './scores.css';
import store from '../../store';
import RefereeService from "../../services/referee.service";
import CategoryOverview from "./CategoryOverview";
import PointColumn from './PointColumn';
import { setCourt } from '../../actions/court';
import { setRegistrationId, setRoundId, setCategory, setCategoryId } from '../../actions/registration';

export default function ScoresOverview(props) {

  const courtId = useSelector((state) => state.court.court);
  const history = useHistory();
  const registration = useSelector((state) => state.registration);
  const categoryId = registration.categoryId;

  const [allRegistrations, setAllRegistrations] = useState([]);
  const [club, setClub] = useState("Club");
  const [name, setName] = useState([]);
  const [scores, setScores] = useState([]);
  const [totalPoints, setTotalPoints] = useState(0);
  const [fiveScores, toggleScores] = useState(false);

  useEffect(() => {
    if (courtId !== 0) {
      RefereeService.getCurrentCategory(courtId).then(
        response => {
          if (response.data[0] !== undefined) {
            let catid = response.data[0].categoryId;
            store.dispatch(setCategoryId(catid));
            //Get Name of Active Category
            RefereeService.getCategoryName(catid).then(
              response => {
                let cat = response.data[0].category + " " + response.data[0].class.classname + " " + response.data[0].class.discipline + " " + response.data[0].class.composition;
                store.dispatch(setCategory(cat));
              });
          }
        });
    }
  }, [registration.categoryId]);

  useEffect(() => {
    //Retrieve Active Category
    setName([]);
    //Get Name of Current Athlete
    if (categoryId !== null) {
      RefereeService.getCurrentAthlete(categoryId).then(
        response => {
          if (response.data[0] !== undefined) {
            let athletes = response.data[0].athletes;
            athletes.forEach(athlete => {
              setName(name => [...name, athlete.name]);
            });
            //setName(response.data[0].athletes[0].name);
            let clubid = response.data[0].athletes[0].clubId;
            store.dispatch(setRegistrationId(response.data[0].id));
            store.dispatch(setRoundId(response.data[0].status));
            //Get Clubname of the Current Athlete
            RefereeService.getClubname(clubid).then(
              response => {
                setClub(response.data.clubname);
              }
            );
          }
          else {
            RefereeService.getRegistrations(categoryId).then(
              response => {
                setAllRegistrations(response.data);
              });
          }
        });
    }
  }, [registration.round]);

  useEffect(() => {
    const socket = io((process.env.NODE_ENV === "production") ? "https://jeomju.tbs" : "http://localhost:8080", { //
      query: {
        court: "court" + courtId,
      },
    });

    // handle new Scores
    socket.on("postedScore", data => {
      //setScores(scores => [...scores, data]);
      setScores(scores => [...scores, [data.refereeName, data.technique, data.presentation, Math.round((data.technique+data.presentation) * 10) / 10, false]]);
      setTotalPoints(0);
    });

    // handle new Registration
    socket.on("changedRegistration", data => {
      if (data.status === 1 || data.status === 2) {
        setName([]);
        store.dispatch(setRoundId(data.status));
      }
      else {
        store.dispatch(setRoundId("/"));
        setName(['No Active Athlete(s)']);
        setClub("No Club");
        getRegistrations();
      }
      setScores([]);
    });

    socket.on("5 Scores detected", () => {
      toggleScores(fiveScores => !fiveScores);
      //console.log("5 Scores detected");
    });

    // handle new Schedule (Category)
    socket.on("changedSchedule", data => {
      store.dispatch(setCategoryId(data.categoryId));
      RefereeService.getRegistrations(data.categoryId).then(
        response => {
          //console.log(response.data);
          setAllRegistrations(response.data);
        });
    });

    // disconnect socket at componentWillUnmount
    return () => socket.disconnect();
  }, []);


/*  useEffect(() => {
    //console.log(response);
    let points = {};
    response.forEach(element => {
      points += element.technique + element.presentation;
    });
    setTotalPoints((points) / response.length);
  }, [response]); */

  useEffect(() => {
    //console.log("enter useeffekt");
    let points = [];
    scores.forEach(element => {
      points.push(element[3]);
      //points += element.technique + element.presentation;
    });
    //console.log(points);
    let sort = points.slice().sort();
    let strike1 = points.indexOf(sort[0]);
    let strike2 = points.lastIndexOf(sort[4]);

    points.sort(); //Array sortieren
    points.shift(); // Erstes Element entfernen
    points.pop(); //Letztes Element entfernen
    let value = points.reduce((a, b) => a + b, 0);

    setTotalPoints(Math.round(((value) / (scores.length-2))*100)/100);

    if(scores.length >= 5){
      let scoresCopy = scores;
      for( let i = 0; i < scores.length; i++){
        if(i === strike1 || i === strike2){
          scoresCopy[i][4] = true;
        }
      } 
      setScores(scoresCopy);
      //gitconsole.log(scoresCopy);
      //setScores(scoresCopy.sort());
    }
  }, [fiveScores]);

  /* const OnClickBack = (event) => {
    store.dispatch(setCourt(0));
    history.push("/scores");
  };
 */
  const getRegistrations = () => {
    RefereeService.getRegistrations(categoryId).then(
      response => {
        //console.log("registrations:");
        //console.log(response.data);
        setAllRegistrations(response.data);
      });
  }

  return (
    <div>
      <h2>{registration.category}</h2>
      {
        (registration.round === "/") ? <div className="athleteInformation"><CategoryOverview registrations={allRegistrations} /></div> : 
        <div className="athleteInformation">
          {
            name.map(n => (<h3 key={n}>{n}</h3>))
          }
          <Divider/>
          {/* <h3>{name}</h3> */}
          <h3>{club}</h3>
          <h3>Durchgang: {registration.round}</h3>
          <div className="scores">
          <div>
            <div>R</div>
            <div>T</div>
            <div>P</div>
            <div>∑</div>
          </div>
            {/* <div> */}
              {
                (scores.length !== 0) ? (scores.map(col => (
                  <PointColumn key={scores.indexOf(col)} column={col} ></PointColumn>))) : ""
              }
           {/*  </div> */}
            <div className="total">
              {(scores.length < 5) ? "" : <h1 className="textHighlihted">{totalPoints}</h1>}
            </div>
          </div>
        </div>
      }

      {/*<Button variant="contained" color="primary" onClick={OnClickBack}>Zurück</Button>*/}
    </div>
  );
}
