import Divider from '@material-ui/core/Divider';
import { useEffect, useState } from 'react';
import RefereeService from "../../services/referee.service";
import { useSelector } from 'react-redux';

export default function Scores(props) {

    const [firstRoundScore, setFirstRoundScores] = useState(0);
    const [secondRoundScore, setSecondRoundScores] = useState(0);
    const [totalPoints, setTotalPoints] = useState(0);
    const registration = useSelector((state) => state.registration);

    useEffect(() => {
        RefereeService.getScores(parseInt(props.registrationId), 1).then(res => {
            //console.log(res);
            calcPoints(res.data, setFirstRoundScores);
            if(!registration.category.includes("Freestyle")){
                RefereeService.getScores(parseInt(props.registrationId), 2).then(res => {
                    calcPoints(res.data, setSecondRoundScores);
                });
            }
        });
    });

    useEffect(() => {
        if(registration.category.includes("Freestyle")){
            setTotalPoints(Math.round(firstRoundScore * 100) / 100);
          
        }
        else{
            setTotalPoints(Math.round(((firstRoundScore + secondRoundScore) / 2) * 100) / 100);
        }
    },[firstRoundScore, secondRoundScore]);


    const calcPoints = (scores, setPoints) => {

        if (scores.length !== 0) {

            let points = [];
            scores.forEach(element => {
                points.push(Math.round((element.technique + element.presentation) * 10) / 10);
            });
            points.sort(); //Array sortieren
            points.shift(); // Erstes Element entfernen
            points.pop(); //Letztes Element entfernen
            let value = points.reduce((a, b) => a + b, 0)
            setPoints(Math.round(((value) / (scores.length - 2)) * 10) / 10);
        }
    };

    return (
        <div >
            <div className="scoresOverview">
                <div>
                    {props.athletes.map(a => <div>{a.name}</div>)} 
                </div>
                <div>
                    {totalPoints}
                </div>
            </div>
            <Divider />
        </div>
    );
}