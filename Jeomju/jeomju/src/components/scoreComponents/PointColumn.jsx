import React from "react";
import Divider from '@material-ui/core/Divider';

export default function PointColumn(props) {
  return (
    
    <div className={props.column[4] ? "strike" : ""}>
      <div>
        {props.column[0]}
      </div>
      <div>
        {props.column[1]}
      </div>
      <div>
        {props.column[2]}
      </div>
      {/* <Divider/> */}
      <div className="divider">
        {/* {Math.round((props.technique + props.presentation) * 10) / 10} */}
        {props.column[3]}
      </div>
    </div>
  )
}