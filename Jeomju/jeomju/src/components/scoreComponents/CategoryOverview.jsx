import Scores from "./Scores";

export default function CategoryOverview (props ){

    return (
      <div>
        {
          props.registrations.map(r => <Scores key={r.id} registrationId={r.id} athletes={r.athletes} />)
            //(<div><h3>{r.athletes.map(a => (<p>{a.name}</p>)) }</h3><Divider/></div>))
        }
      </div>
    );
  }