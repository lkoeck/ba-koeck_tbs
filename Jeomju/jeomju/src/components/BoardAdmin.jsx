import React from "react";
//import { useEffect, useState } from "react";
import { useHistory } from "react-router-dom";
import { useSelector } from 'react-redux';
import Button from '@material-ui/core/Button';
import FormControl from '@material-ui/core/FormControl';
import InputLabel from '@material-ui/core/InputLabel';
import Select from '@material-ui/core/Select';

import "./adminRefereeComponents/adminComponents.css"
import store from '../store';
import { setCourt } from '../actions/court';
//import UserService from "../services/user.service";


export default function BoardAdmin(props) {

  //const [content, setContent] = useState("");
  const courtId = useSelector((state) => state.court.court);
  const history = useHistory();

  const onChange = (event) => {
    store.dispatch(setCourt(event.target.value));
  };
  const onClick = (event) => {
    history.push("/timeTable");
  };

  /* useEffect(() => {
    UserService.getAdminBoard().then(
      response => {
        setContent(response.data)
      },
      error => {
        setContent((error.response &&
          error.response.data &&
          error.response.data.message) ||
          error.message ||
          error.toString())
      }
    );å
  }, []); */

  return (
    <div className="container">
      <header className="jumbotron box">
        {/* <h3>{content}</h3> */}
        <h2>Hello Admin, please select your court!</h2>
        <div className="selectCourtAdmin">
          <FormControl variant="outlined">
            <InputLabel htmlFor="outlined-age-native-simple">Court</InputLabel>
            <Select
              native
              value={courtId}
              onChange={onChange}
              label="Court"
              inputProps={{
                name: 'court_id',
                id: 'outlined-age-native-simple',
              }}
            >
              <option aria-label="None" value="" />
              <option value={1}>Court 1</option>
              <option value={2}>Court 2</option>
              <option value={3}>Court 3</option>
            </Select>
          </FormControl>
          <Button variant="contained" color="primary" onClick={onClick}>
            Let the Game begin
          </Button>
        </div>
      </header>
    </div>
  );

}

/*
import React, { Component } from "react";

import UserService from "../services/user.service";

export default class BoardUser extends Component {
  constructor(props) {
    super(props);

    this.state = {
      content: ""
    };
  }

  componentDidMount() {
    UserService.getAdminBoard().then(
      response => {
        this.setState({
          content: response.data
        });
      },
      error => {
        this.setState({
          content:
            (error.response &&
              error.response.data &&
              error.response.data.message) ||
            error.message ||
            error.toString()
        });
      }
    );
  }

  render() {
    return (
      <div className="container">
        <header className="jumbotron">
          <h3>{this.state.content}</h3>
        </header>
      </div>
    );
  }
}*/