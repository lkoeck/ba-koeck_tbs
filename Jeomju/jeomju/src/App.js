import React, { useEffect} from "react";
import { connect } from "react-redux";
import { history } from './helpers/history';
import { Link, Route, Router, Switch } from "react-router-dom";

import "bootstrap/dist/css/bootstrap.min.css";
import "./App.css";
import "./navbar.css";
import { clearMessage } from "./actions/message";
import { logout } from "./actions/auth";

import Login from "./components/Login";
import Register from "./components/Register";
import Home from "./components/Home";
import BoardAdmin from "./components/BoardAdmin";
import BoardReferee from "./components/BoardReferee";
import BoardScores from "./components/BoardScores";
import ScoresOverview from "./components/scoreComponents/ScoresOverview";
import PoomsaeTechniquePoints from "./components/refereeComponents/PoomsaeTechniquePoints";
import PoomsaePresentationPoints from "./components/refereeComponents/PoomsaePresentationPoints";
import PoomsaeOverviewPoints from "./components/refereeComponents/PoomsaePointsOverview";
import FSTechniquePoints from "./components/refereeComponents/FreestyleTechniquePoints";
import FSPresentationPoints from "./components/refereeComponents/FreestylePresentationPoints";
import FSOverviewPoints from "./components/refereeComponents/FreestylePointsOverview";
import CurrentAthlete from "./components/refereeComponents/CurrentAthlete";
import TimeTable from  "./components/adminRefereeComponents/TimeTable";


function App(props){
  const [state, setState] = React.useState({
    showRefereeBoard: false,
    showAdminBoard: false,
    currentUser: undefined,
  });

  history.listen((location) => {
    props.dispatch(clearMessage()); // clear message when changing location
  });

  useEffect(() => {
    const user = props.user;

    if (user) {
      setState({
        currentUser: user,
        showRefereeBoard: user.role === "referee" ? true : false,
        showAdminBoard: user.role === "admin" ? true : false,
      });
    }
  }, [props.user]);

  function logOut(){
    props.dispatch(logout());
  };

  return (
    <Router history={history}>
      <div>
        <nav className="navbar navbar-expand navbar-dark bg-dark">
          <Link to={"/"} className="navBrand">
          점수 Jeomju
          </Link>
          <div className="navbarNav">
            <li className="nav-item">
              <Link to={"/home"} className="navLink">
                Home
              </Link>
            </li>

            <li className="nav-item">
              <Link to={"/scores"} className="navLink">
                Scores
              </Link>
            </li>

            {state.showRefereeBoard && (
              <li className="nav-item">
                <Link to={"/referee"} className="navLink">
                  Referee Board
                </Link>
              </li>
            )}

            {state.showAdminBoard && (
              <li className="nav-item">
                <Link to={"/admin"} className="navLink">
                  Admin Board
                </Link>
              </li>
            )}

          

          </div>

          {state.currentUser ? (
            <div className="navbar-nav ml-auto">
              <li className="nav-item">
                <Link to={"/home"} className="navLink">
                  {state.currentUser.username}
                </Link>
              </li>
              <li className="nav-item">
                <a href="/login" className="navLink" onClick={logOut}>
                  Logout
                </a>
              </li>
            </div>
          ) : (
            <div className="navbar-nav ml-auto">
              <li className="nav-item">
                <Link to={"/login"} className="navLink">
                  Login
                </Link>
              </li>

              <li className="nav-item">
                <Link to={"/register"} className="navLink">
                  Sign Up
                </Link>
              </li>
            </div>
          )}
        </nav>
        <div className="container mt-3">
          <Switch>
            <Route exact path={["/", "/home"]} component={Home} />
            <Route exact path="/login" component={Login} />
            <Route exact path="/register" component={Register} />
            <Route path="/referee" component={BoardReferee} />
            <Route path="/admin" component={BoardAdmin} />
            <Route path="/poomsae/technique" component={PoomsaeTechniquePoints}/>
            <Route path="/poomsae/presentation" component={PoomsaePresentationPoints}/>
            <Route path="/poomsae/overview" component={PoomsaeOverviewPoints}/>
            <Route path="/freestyle/technique" component={FSTechniquePoints}/>
            <Route path="/freestyle/presentation" component={FSPresentationPoints}/>
            <Route path="/freestyle/overview" component={FSOverviewPoints}/>
            <Route path="/currentAthlete" component={CurrentAthlete}/>
            <Route path="/timeTable" component={TimeTable}/>
            <Route path="/scores" component={BoardScores}/>
            <Route path="/scoreOverview" component={ScoresOverview}/>
          </Switch>
        </div>
      </div>
    </Router>
  );
}
export default connect(mapStateToProps)(App);

function mapStateToProps(state) {
  const { user } = state.auth;
  return {
    user,
  };
}


/*class App extends Component {
  constructor(props) {
    super(props);
    this.logOut = this.logOut.bind(this);

    this.state = {
      showRefereeBoard: false,
      showAdminBoard: false,
      currentUser: undefined,
    };

    history.listen((location) => {
      props.dispatch(clearMessage()); // clear message when changing location
    });
  }

  componentDidMount() {
    const user = this.props.user;

    if (user) {
      this.setState({
        currentUser: user,
        showRefereeBoard: user.role === "referee" ? true : false,
        showAdminBoard: user.role === "admin" ? true : false,
      });
    }
  }

  logOut() {
    this.props.dispatch(logout());
  }

  render() {
    const { currentUser, showRefereeBoard, showAdminBoard } = this.state;

    return (
      <Router history={history}>
        <div>
          <nav className="navbar navbar-expand navbar-dark bg-dark">
            <Link to={"/"} className="navbar-brand">
              Jeomju
            </Link>
            <div className="navbar-nav mr-auto">
              <li className="nav-item">
                <Link to={"/home"} className="nav-link">
                  Home
                </Link>
              </li>

              {showRefereeBoard && (
                <li className="nav-item">
                  <Link to={"/referee"} className="nav-link">
                    Referee Board
                  </Link>
                </li>
              )}

              {showAdminBoard && (
                <li className="nav-item">
                  <Link to={"/admin"} className="nav-link">
                    Admin Board
                  </Link>
                </li>
              )}

            </div>

            {currentUser ? (
              <div className="navbar-nav ml-auto">
                <li className="nav-item">
                  <Link to={"/profile"} className="nav-link">
                    {currentUser.username}
                  </Link>
                </li>
                <li className="nav-item">
                  <a href="/login" className="nav-link" onClick={this.logOut}>
                    LogOut
                  </a>
                </li>
              </div>
            ) : (
              <div className="navbar-nav ml-auto">
                <li className="nav-item">
                  <Link to={"/login"} className="nav-link">
                    Login
                  </Link>
                </li>

                <li className="nav-item">
                  <Link to={"/register"} className="nav-link">
                    Sign Up
                  </Link>
                </li>
              </div>
            )}
          </nav>
          <div className="container mt-3">
            <Switch>
              <Route exact path={["/", "/home"]} component={Home} />
              <Route exact path="/login" component={Login} />
              <Route exact path="/register" component={Register} />
              <Route path="/referee" component={BoardReferee} />
              <Route path="/admin" component={BoardAdmin} />
            </Switch>
          </div>
        </div>
      </Router>
    );
  }
}

function mapStateToProps(state) {
  const { user } = state.auth;
  return {
    user,
  };
}

export default connect(mapStateToProps)(App);*/